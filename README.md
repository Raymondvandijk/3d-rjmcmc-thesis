# 3D RJMCMC localization thesis


## Introduction

This repository contains the code to my 3D RJMCMC localization algorithm. The algorithm is provided in moves_release.py, using priors from priors_release.py. Additional example code to localize two overlapping emitters is provided in two_emitter_demo.py. To run the code, download the photonpy library (provided by J. Cnossen) and run setup.py. Download the modules and run two_emitter_demo.py. 

Alternatively, the code can be run with Jupyter notebook on google collab from the following links:
- [ ] https://colab.research.google.com/github/qnano/rjmcmc3D/blob/master/colab_example.ipynb uses a Gaussian PSF model
- [ ] https://colab.research.google.com/github/qnano/rjmcmc3D/blob/master/colab_example_cspline.ipynb uses a C-spline PSF model


## Documentation

The code is based on my thesis work at the Smith optics lab of Delft center for systems and control. The manuscript and supporting information are also provided as pdf files.


