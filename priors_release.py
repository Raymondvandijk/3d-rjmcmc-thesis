import numpy as np


def gauss1d(x, mean=0,sigma=1):
    """
    Return probabilities P(x1), P(x2), ... P(xn) of samples x1, x2, ... xn in 
    array x assuming underlying Gaussian distribution N(mean, sigma)

    Parameters
    ----------
    x :     1d array 
            Samples for evaluating the probability following a Gaussian distribution.
        
    mean : float
            Mean of the Gaussian distribution.
            The default is 0.
            
    sigma : float
            Standard deviation of the Gaussian distribution.
            The default is 1.

    Returns
    -------
            Array of probabilities [P(x1), P(x2), ... P(xn)]

    """
    
    return np.exp((-(x-mean)**2)/(2*sigma**2))/(np.sqrt(2*np.pi)*sigma)


def prior_k(emitterState, k_PDF, k_max):
    """
    Evaluate the prior probability of n_eval models in emitterState given model
    prior distribution k_PDF and maximum emitter count k_max.

    Parameters
    ----------
    emitterState :  array, [n_eval, k_max]
                    Currently active emitters for n_eval frames, up to k_max emitters.
                    
    k_PDF :         array, [k_max]
                    Probability density function for distribution of model parameters.
                    
    k_max :         float
                    Maximum amount of active emitters.

    Returns
    -------
    p_k :           array, [n_eval]
                    Prior probability of the model.
    """
    
    # initialize prior probabilities
    p_k = np.zeros(np.shape(emitterState)[0])
    
    # sum active emitters
    K = np.array(emitterState.sum(1), dtype = int)
    
    # limits, find prior from k_PDF corresponding to K
    k_PDF2 = np.concatenate([np.copy(k_PDF),np.array([0])],axis=0)
    K[(K < 1)] = 0
    K[(K > k_max)] = 0
    np.putmask(p_k, (K >= 0) & (K <= k_max), k_PDF2[K])
    
    return p_k

def prior_I(I, emitterState, I_PDF):
    """
    Evaluate the prior probability of emitter intensity for n_eval active 
    emitters in emitterState with intensity I given intensity prior 
    distribution I_PDF.

    Parameters
    ----------
    I :             array, [n_eval, k_max, numFrames]
                    Current emitter intensity for n_eval frames, up to k_max emitters, with numFrames shot in parallel.
                            
    emitterState :  array, [n_eval, k_max]
                    Currently active emitters for n_eval frames, up to k_max emitters.
                    
    I_PDF :         array, [I_max]
                    Probability density function for distribution of model parameters.

    Returns
    -------
                    array, [n_eval]
                    Prior probability of emitter intensity.

    """
    # integers for indexing the lookup table I_PDF
    I2 = I.astype(int)
    
    # initialize prior probability
    p_I = np.zeros(np.shape(I2))
    
    # mask for active emitters
    es2 = np.zeros(I.shape)
    es2[:,:,:] = ~emitterState[:,:,None]
    
    # limits, return joint probability of intensity for parallel frames
    I2[(I2 > len(I_PDF))] = -1
    I2[(I2 < 0)] = 0
    I2[(I2 >= len(I_PDF))] = 0
    np.putmask(p_I, (I2 < len(I_PDF)) & (I2 >= 0), I_PDF[I2])
    np.putmask(p_I, es2, 1)
    
    return p_I.prod(2)

def prior_bg(bg_in, bg_PDF):
    """
    Evaluate the prior probability of background intensity for n_eval frames in
    bg_in given background intensity prior distribution bg_PDF.

    Parameters
    ----------
    bg_in :     array, [n_eval]
                Current background intensity for n_eval frames.
                
    bg_PDF :    array, [bg_max]
                Probability density function for distribution of background intensity.
                

    Returns
    -------
    p_bg :      array, [n_eval]
                Prior probability of background intensity.

    """
    
    # integers for indexing the lookup table bg_PDF
    bg = bg_in.astype(int)
    
    # initialize prior probability
    p_bg= np.zeros(np.shape(bg))
    
    # limits, return prior probability of background intensity
    bg[bg < 0] = -1
    np.putmask(p_bg, bg >= 0, bg_PDF[bg])
    
    return p_bg
    
def prior_xyz(pos, emitterState, roisize, border, zmin, zmax):
    """
    Evaluate the prior probability of emitter position for n_eval frames using
    positions in pos for active emitters in emitterState. Roisize, border, 
    zmin, and zmax define the bounds of the 3D emitter position.

    Parameters
    ----------
    pos :           array, [n_eval, k_max, dim]
                    Current position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                    
    emitterState :  array, [n_eval, k_max]
                    Currently active emitters for n_eval frames, up to k_max emitters.
                    
    roisize :       float
                    Size of the (square) region of interest, in pixels.
                    
    border :        float
                    Additional pixels outside the region of interest to consider for position estimates.
                    
    zmin :          float
                    Minimum emitter axial position w.r.t. focal plane.
        
    zmax :          float
                    Maximum emitter axial position w.r.t. focal plane.

    Returns
    -------
    p_xy : TYPE
        DESCRIPTION.

    """
    # read x y z from pos array
    x = pos[:,:,0]
    y = pos[:,:,1]
    z = pos[:,:,2]
    
    # initialize priors
    p_xy = np.zeros(np.shape(x))
    
    # mask according to limits, return prior
    np.putmask(p_xy,(x > -border) & (x < roisize + border) & (y > -border) & (y < roisize + border) & (z > zmin) & (z < zmax), 1/(roisize + 2*border)**2/(zmax - zmin))  
    np.putmask(p_xy, ~emitterState, 1)  
    
    return p_xy

class Prior_calculator:
    """
    The prior calculator is used to initialize all prior functions and calculate
    the prior probabilities P(\theta | k) and P(k) for use in the acceptance
    rate of the algorithm.
    """
    def __init__(self, n_eval, pI_func, I_PDF, pbg_func, bg_PDF, pxy_func, roisize, border, zmin, zmax, pk_func, k_PDF, mu_k, k_max):
        self.pI_func    = pI_func       # emitter intensity prior calculator
        self.I_PDF      = I_PDF         # emitter intensity prior distribution
        self.pbg_func   = pbg_func      # background intensity prior calculator
        self.bg_PDF     = bg_PDF        # background intensity prior distribution
        self.pxy_func   = pxy_func      # emitter position prior calculator
        self.roisize    = roisize       # frame region of interest
        self.border     = border        # frame border to account for emitters outside the region of interest
        self.zmin       = zmin          # emitter minimum distance from focal plane
        self.zmax       = zmax          # emitter maximum distance from focal plane
        self.pk_func    = pk_func       # model prior calculator
        self.k_PDF      = k_PDF         # model prior distribution
        self.mu_k       = mu_k          # OBSOLETE 
        self.k_max      = k_max         # maximum number of emitters
        self.n_eval     = n_eval        # number of frames
        self.a          = 0             # OBSOLETE
        self.z0         = 0             # OBSOLETE
        
    def calc(self, pos, I, backgroundParams, emitterState, loc = []):
        """
        Calculate the prior probability of the estimate given the current 
        position in pos, intensity in I, background in backgroundParams, and 
        active emitters in emitterState.

        Parameters
        ----------
        pos :               array, [n_eval, k_max, dim]
                            Current position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                       
        I :                 array, [n_eval, k_max, numFrames]
                            Current emitter intensity for n_eval frames, up to k_max emitters, with numFrames shot in parallel.
                            
        backgroundParams :  array, [n_eval]
                            Current background photon count for n_eval frames. 
                            
        emitterState :      array, [n_eval, k_max]
                            Currently active emitters for n_eval frames, up to k_max emitters.
                            
        loc :               OBSOLETE

        Returns
        -------
                            array, [n_eval]
                            Array of joint parameter and model prior probabilities for n_eval frames.

        """
        # evaluate log intensity, background, position, and model prior
        pi_o = np.log(self.pI_func(I, emitterState, self.I_PDF))
        pbg_o = np.log(self.pbg_func(backgroundParams, self.bg_PDF))
        pxy_o = np.log(self.pxy_func(pos, emitterState, self.roisize, self.border, self.zmin, self.zmax))
        pk_o = np.log(self.pk_func(emitterState, self.k_PDF, self.k_max))
        
        # sum log priors of intensity and position over emitters in frame
        return (pi_o + pxy_o).sum(1) + pbg_o + pk_o 
    
    def prior_mat(self, pos, I, backgroundParams, emitterState):
        """
        OBSOLETE
        """
        priors = np.zeros([pos.shape[0], 4],dtype=float)
        # pI, pxy, pbg, pk
        priors[:,0] = np.log(self.pI_func(I, emitterState, self.I_PDF)).sum(1)
        priors[:,1] = np.log(self.pxy_func(pos, emitterState, self.roisize, self.border, self.zmin, self.zmax)).sum(1)
        priors[:,2] = np.log(self.pbg_func(backgroundParams, self.bg_PDF))
        priors[:,3] = np.log(self.pk_func(emitterState, self.k_PDF, self.k_max))
        return priors
