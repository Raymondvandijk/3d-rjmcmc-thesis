#%% import modules

import numpy as np
import ctypes as ct
import numpy.ctypeslib as ctl
from scipy.stats import poisson, beta

# compact psf evaluation module from the photonpy library
#from fastpsf import Context, Estimator
from photonpy import Context, Estimator

# classes and functions to determine prior probability of estimates, by R. Dijk (author)
from priors_release import gauss1d, Prior_calculator, prior_I, prior_xyz, prior_bg, prior_k

#%% Interfacing classes by J. Cnossen
"""
This section includes some additional classes for interacting with the 
photonpy library, provided by J. Cnossen. 

PDFInfo can be used to store a prior distribution. 

MCLocalizer is used to effectively compute Likelihoods for frames with
multiple active emitters. The PSF, priors, and boundary conditions are set
upon initalization. MCLocalizer.SetSamples is used to assign image data
to the localizer, while MCLocalizer.Likelihood returns the Likelihood of the
current estimate given the image data.
"""

class PDFInfo(ct.Structure):
    _fields_ = [
        ("pdf", ctl.ndpointer(np.float32, flags="aligned, c_contiguous")),
        ("size", ct.c_int32),
        ("start", ct.c_float),
        ("end", ct.c_float),
    ]

    def __init__(self, pdf, start=0, end=1):
        pdf = np.ascontiguousarray(pdf, dtype=np.float32)
        
        # normalize (elements of pdf*step should sum up to 1)
        step = (end-start) / len(pdf)
        pdf = pdf / (pdf.sum() * step)
        
        self.step = step
        self._pdf = pdf # Hack to make sure the array doesn't get GCed
        self.pdf = pdf.ctypes.data
        self.start = start
        self.end = end


class MCLocalizer:
    def __init__(self, maxEmitters, numFrames, psf:Estimator, intensityPDF:PDFInfo, ctx:Context=None):
        self.ctx = ctx
        lib = self.ctx.smlm.lib

        #MCLocalizer* MCL_Create(int maxEmitters, int nframes, Estimator* psf, const PDFInfo* intensity_pdf);
        self._MCL_Create = lib.MCL_Create
        self._MCL_Create.argtypes = [
            ct.c_void_p,
            ct.c_int32,
            ct.c_int32,
            ct.c_void_p,
            ct.POINTER(PDFInfo)
        ]
        self._MCL_Create.restype = ct.c_void_p
        #void MCL_Destroy(MCLocalizer* mcl;
        
        self._MCL_Destroy = lib.MCL_Destroy
        self._MCL_Destroy.argtypes =[
            ct.c_void_p,
            ]

        self.paramsDType = np.dtype([
            ('pos','<f4', (3,)),
            ('I','<f4', (numFrames,))
        ])
        
        #void MCL_SetSamples(MCLocalizer* mcl, const float* data, const float* background, int numROIs);
        self._MCL_SetSamples = lib.MCL_SetSamples
        self._MCL_SetSamples.argtypes =[
            ct.c_void_p,
            ctl.ndpointer(np.float32, flags="aligned, c_contiguous"), 
            ctl.ndpointer(np.float32, flags="aligned, c_contiguous"), 
            ct.c_int32
            ]
        
        #void MCL_ComputeLikelihood(MCLocalizer* mcl, float* ll, const float* params, bg, const int* sampleIndices, int numEvals);
        self._MCL_ComputeLikelihood = lib.MCL_ComputeLikelihood
        self._MCL_ComputeLikelihood.argtypes =[
            ct.c_void_p,
            ctl.ndpointer(np.float32, flags="aligned, c_contiguous"), 
            ctl.ndpointer(self.paramsDType, flags="aligned, c_contiguous"), 
            ctl.ndpointer(np.float32, flags="aligned, c_contiguous"),  # bg
            #ctl.ndpointer(np.float32, flags="aligned, c_contiguous"), 
            ctl.ndpointer(np.int32, flags="aligned, c_contiguous"), 
            ct.c_int32
            ]
        
        #void MCL_ComputeExpVal(MCLocalizer* mcl, float* expval, const float* params, int numEvals);
        self._MCL_ComputeExpVal = lib.MCL_ComputeExpVal
        self._MCL_ComputeExpVal.argtypes =[
            ct.c_void_p,
            ctl.ndpointer(np.float32, flags="aligned, c_contiguous"), 
            #ctl.ndpointer(np.float32, flags="aligned, c_contiguous"), 
            ctl.ndpointer(self.paramsDType, flags="aligned, c_contiguous"), 
            ct.c_int32
            ]
        
        self.maxEmitters = maxEmitters
        self.numFrames = numFrames
        self.psf = psf
        self.inst = self._MCL_Create(ctx.inst, maxEmitters, numFrames, psf.inst, intensityPDF)
        
    
    def Destroy(self):
        if self.inst is not None:
            self._MCL_Destroy(self.inst)
            self.inst = None

    def MakeParams(self, emitterPos, emitterI):
        numEval = emitterPos.shape[0]
        params = np.zeros((numEval, self.maxEmitters),dtype=self.paramsDType)
        params['pos'] = emitterPos
        params['I'] = emitterI
        return params
            
    def Likelihood(self, emitterPos, emitterI, backgrounds, sampleIndices):
        assert len(emitterPos) == len(sampleIndices)
        assert np.array_equal(backgrounds.shape,[len(emitterPos)])

        backgrounds = np.ascontiguousarray(backgrounds,dtype=np.float32)        
        sampleIndices = np.ascontiguousarray(sampleIndices,dtype=np.int32)
        params = self.MakeParams(emitterPos, emitterI)
        ll = np.zeros(len(params), dtype=np.float32)
        self._MCL_ComputeLikelihood(self.inst, ll, params, backgrounds, sampleIndices, len(params))
        return ll
            
    def SetSamples(self, samples, backgrounds):
        """
        Every sample has an associated background estimate, 
        in practice typically from a temporal median filter over a series of frames
        
        samples: [numsmp, numframes, psf samplecount]
        backgrounds: same as samples
        """
        samples = np.ascontiguousarray(samples,dtype=np.float32)
        backgrounds = np.ascontiguousarray(backgrounds,dtype=np.float32)
        assert np.array_equal(samples.shape, [samples.shape[0], samples.shape[1], *self.psf.sampleshape])
        assert np.array_equal(samples.shape, backgrounds.shape)
        
        self.samples = samples
        self._MCL_SetSamples(self.inst, samples, backgrounds, len(samples))
        
        
            
    def ExpectedValue(self, emitterPos, emitterI):
        """
        emitterPos: [numEval, maxEmitters, 3]
        emitterI: [numEval, maxEmitters, numFrames]
        
        sampleIndices  
        """ 
        
        params = self.MakeParams(emitterPos, emitterI)
        
        expval = np.zeros((len(params), self.numFrames, *self.psf.sampleshape), dtype=np.float32)
        self._MCL_ComputeExpVal(self.inst, expval, params, len(params))
        
        return expval
    
    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.Destroy()

#%% RJMCMC functions and classes by R. Dijk (author)
"""
This section defines the main functions and classes of the RJMCMC localization 
algorithm.

Function ClosestMulti2 finds clusters of emitters for use in grouped RJMCMC 
moves.

Classes Single_Emitter_Move, Group_Move_Unconserved, and Background_Move define
the moves that exclusively update the parameter space. Split, 
Generalized_Split, Merge, Generalized_Merge, Birth, and Death define the moves
that update both the model and parameters.

The rjmcmc_3d_localization function executes the algorithm by iteratively 
making jumps from using the provided moves, finally returning the output Markov 
chains after a fixed number of iterations.

The RJMCMC_Object class is used to initialize the algorithm, execute the 
rjmcmc_3d_localization function for the RJMCMC and MCMC runs, find the model, 
and collect the data.
"""


def ClosestMulti2(pos, emitterState, loc, emIndices, d_max, pixelSize = 100, z_rescale = 1):
    """
    This function is used for clustering in the grouped RJMCMC moves (split, 
    merge, group move). It uses the current model and parameter estimates to 
    find all emitters within d_max of the emitter indicated by loc.

    Parameters
    ----------
    pos :           array, [n_eval, k_max, dim]
                    Current position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                    
    emitterState :  array, [n_eval, k_max]
                    Currently active emitters for n_eval frames, up to k_max emitters.
                    
    loc :           array, [n_eval]
                    n_eval frames to operate on.
                    
    emIndices :     array, [n_eval]
                    n_eval indices pointing to the active emitter at the center of the cluster.
                    
    d_max :         float
                    Cluster radius.
                    
    pixelSize :     float
                    Assumed pixel size for ROI in nanometers, used to form a spherical cluster.
                    The default is 100.
                    
    z_rescale :     float
                    Tuning parameter used to stretch or compress the clustering sphere into an ellipsoid.
                    The default is 1.

    Returns
    -------
    Closest :       array, [n_eval, k_max]
                    Array indicating all emitters within the cluster for n_eval frames, up to k_max emitters.

    """
    
    n_eval = emitterState.shape[0]
    #maxEmitters = emitterState.shape[1]
    
    emstate_temp = np.copy(emitterState)
    emstate_temp[loc, emIndices] = False
    mp_temp = emstate_temp.sum(1)
    
    # positions of the emitters to be clustered around
    x0 = pos[loc, emIndices, 0]
    y0 = pos[loc, emIndices, 1]
    z0 = pos[loc, emIndices, 2]
    
    # initialize arrays to store squared distance in x, y, and z directions
    # of each emitter to the cluster center
    x2 = np.zeros(emitterState.shape)
    y2 = np.zeros(emitterState.shape)
    z2 = np.zeros(emitterState.shape)
    x2[:,:] = np.nan
    y2[:,:] = np.nan
    z2[:,:] = np.nan

    # for selected frames, calculate squared xyz distance
    for i in range(len(loc)):
        if mp_temp[i] != 0:
            x2[i,emstate_temp[i]] = np.square(pos[i, emstate_temp[i],0] - x0[i])
            y2[i,emstate_temp[i]] = np.square(pos[i, emstate_temp[i],1] - y0[i])
            z2[i,emstate_temp[i]] = np.square((pos[i, emstate_temp[i],2] - z0[i])*1000/pixelSize*z_rescale)

    # calculate radial distance to cluster center
    d = np.sqrt(x2 + y2 + z2)  
    
    # index emitters within the clustering radius
    Ind = np.zeros(emitterState.shape, dtype = bool)
    Ind[np.where(d < d_max)] = True
    Num = Ind.sum(1)

    # randomly pick some emitters within range to use in the grouped moves
    Np = np.zeros(n_eval, dtype = int)
    Ua = np.random.uniform(0,1,(Num > 0).sum()) 
    Np2 = Np[Num > 0]
    Num2 = Num[Num > 0]
    rand_ind = Ua < 1/Num2
    Np2[rand_ind] = Num2[rand_ind]
    Np2[~rand_ind] = np.amin(((Ua*Num2)[~rand_ind].astype(int), Num2[~rand_ind] - 1), axis = 0)
    
    Np[Num > 0] = Np2
    
    # for sets with Num > 0
    # for 0 < i < Np
    # randomly pick Np elements from Num closest elements to return as cluster
    Closest = np.zeros(emitterState.shape, dtype = bool)

    for i in range(len(loc)):
        Closest[i, np.random.choice(np.where(Ind[i])[0], Np[i], replace = False)] = True
        Closest[i, emIndices[i]] = True
    return Closest


class Single_Emitter_Move:
    """
    This move randomly selects an active emitter. It shifts the current 
    estimated xyz position and photon intensity using a Gaussian distribution.
    The move tests accepts or rejects the new parameters based on the 
    acceptance rate of the Metropolis-Hastings algorithm
    """
    def __init__(self, sigma_x, sigma_y, sigma_z, sigma_I, prior_func, LL_func):
        self.sigma_x = sigma_x                      # standard deviation for sampling the x position
        self.sigma_y = sigma_y                      # standard deviation for sampling the y position
        self.sigma_z = sigma_z                      # standard deviation for sampling the z position
        self.sigma_I = sigma_I                      # standard deviation for sampling the photon intensity I
        self.prior_func = prior_func                # prior information
        self.LL_func = LL_func                      # Likelihood function
        
    def calc(self, pos, I, backgroundParams, emitterState, k_max, smpIndices, ll_prev, prior_prev):
        """
        Executing this function generates new parameters and accepts or rejects
        them. 

        Parameters
        ----------
        pos :               array, [n_eval, k_max, dim]
                            Current position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                       
        I :                 array, [n_eval, k_max, numFrames]
                            Current emitter intensity for n_eval frames, up to k_max emitters, with numFrames shot in parallel.
                            
        backgroundParams :  array, [n_eval]
                            Current background photon count for n_eval frames. 
            
        emitterState :      array, [n_eval, k_max]
                            Currently active emitters for n_eval frames, up to k_max emitters.
                        
        k_max :             float
                            Maximum amount of active emitters.
                            
        smpIndices          array, [n_eval]
                            Informs Likelihood function which frames to use to calculate the Likelihood given the parameters.
                            
        ll_prev :           array, [n_eval]
                            Previous log Likelihood value, belonging to current parameters.
                            
        prior_prev :        array, [n_eval]
                            Previous prior probability value, belonging to current parameters.

        Returns
        -------
        pos_update :        array, [n_eval, k_max, dim]
                            Updated position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                        
        I_update :          array, [n_eval, k_max, numFrames]
                            Updated emitter intensity for n_eval frames, up to k_max emitters, with numFrames shot in parallel.
                            
        backgroundParams :  array, [n_eval]
                            Current background photon count for n_eval frames.
                            
        emitterState :      array, [n_eval, k_max]
                            Currently active emitters for n_eval frames, up to k_max emitters.
                            
        ~ failed_updates :  array, [n_eval]
                            Inverted failed updates returns an array of frames with a successful jump.
                            
        ll_new :            array, [n_eval]
                            New log Likelihood value, belonging to updated parameters.
                            
        prior_new :         array, [n_eval]
                            Previous prior probability value, belonging to current parameters.

        """
        n_eval, km, numFrames = I.shape
        
        # in each frame, pick an emitter to update
        update_indices = np.zeros(n_eval, dtype = int)
        for i in range(n_eval):
            if emitterState[i].sum() != 0:
                update_indices[i] = np.random.choice(np.arange(k_max)[emitterState[i]])     
            else:
                update_indices[i] = 0
                
        # move a single element (x,y,z,I) for each sample
        pos_update = np.copy(pos)
        I_update = np.copy(I)
        pos_update[range(n_eval),update_indices,0] += np.random.normal(0, self.sigma_x, n_eval)
        pos_update[range(n_eval),update_indices,1] += np.random.normal(0, self.sigma_y, n_eval)
        pos_update[range(n_eval),update_indices,2] += np.random.normal(0, self.sigma_z, n_eval)
        
        # update intensities over all frames randomly
        I_update[range(n_eval),update_indices, :] += np.random.normal(0, self.sigma_I, [n_eval, numFrames])
        
        # calculate posterior ratio
        prior_new = self.prior_func.calc(pos_update, I_update, backgroundParams, emitterState, smpIndices)
        ll_new = self.LL_func.Likelihood(pos_update, I_update*emitterState[:,:,None], backgroundParams, smpIndices)

        PR = np.exp(prior_new + ll_new - prior_prev - ll_prev)
        
        # random number for accepting/rejecting the jumps
        u = np.random.uniform(0,1,n_eval)

        # Accept jumps where acceptance ratio > u with u ~ U(0,1)
        # update the estimates
        failed_updates = (PR < u) + np.isnan(PR)        # where posterior ratio < random number AND where posterior ratio isNaN
        pos_update[failed_updates] = pos[failed_updates]        # rejected jumps in each dataset get set back to their original values
        I_update[failed_updates] = I[failed_updates]
        ll_new[failed_updates] = ll_prev[failed_updates]
        prior_new[failed_updates] = prior_prev[failed_updates]
        
        return pos_update, I_update, backgroundParams, emitterState, ~failed_updates, ll_new, prior_new 


class Group_Move_Unconserved:
    """
    This group move shifts individual emitters within a cluster as if executing
    a single emitter move. All eligible emitters within a frame update their
    xyz position and photon intensity with a sample from a Gaussian 
    distribution of appropriate width. The move of the group as a whole is then
    accepted or rejected based on the acceptance rate. This group move does not
    conserve the 'center of mass' (positions weighed by emitter intensity) of 
    the emitters.
    """
    def __init__(self, sigma_PSF, sigma_x, sigma_y, sigma_z, sigma_I, prior_func, LL_func, z_rescale = 1):
        self.sigma_PSF = sigma_PSF                  # PSF width assuming Gaussian or astigmatic PSFs. approximate/tune otherwise.
        self.sigma_x = sigma_x                      # standard deviation for sampling the x position
        self.sigma_y = sigma_y                      # standard deviation for sampling the y position
        self.sigma_z = sigma_z                      # standard deviation for sampling the z position
        self.sigma_I = sigma_I                      # standard deviation for sampling the photon intensity I
        self.prior_func = prior_func                # prior information
        self.LL_func = LL_func                      # Likelihood function
        self.z_rescale = z_rescale                  # flatten/stretch clustering sphere in z direction
        
    def calc(self, pos, I, backgroundParams, emitterState, k_max, smpIndices, ll_prev, prior_prev):
        """
        Executing this function generates new parameters and accepts or rejects
        them. 

        Parameters
        ----------
        pos :               array, [n_eval, k_max, dim]
                            Current position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                       
        I :                 array, [n_eval, k_max, numFrames]
                            Current emitter intensity for n_eval frames, up to k_max emitters, with numFrames shot in parallel.
                            
        backgroundParams :  array, [n_eval]
                            Current background photon count for n_eval frames. 
            
        emitterState :      array, [n_eval, k_max]
                            Currently active emitters for n_eval frames, up to k_max emitters.
                        
        k_max :             float
                            Maximum amount of active emitters.
                            
        smpIndices          array, [n_eval]
                            Informs Likelihood function which frames to use to calculate the Likelihood given the parameters.
                            
        ll_prev :           array, [n_eval]
                            Previous log Likelihood value, belonging to current parameters.
                            
        prior_prev :        array, [n_eval]
                            Previous prior probability value, belonging to current parameters.

        Returns
        -------
        pos_update :        array, [n_eval, k_max, dim]
                            Updated position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                        
        I_update :          array, [n_eval, k_max, numFrames]
                            Updated emitter intensity for n_eval frames, up to k_max emitters, with numFrames shot in parallel.
                            
        backgroundParams :  array, [n_eval]
                            Current background photon count for n_eval frames.
                            
        emitterState :      array, [n_eval, k_max]
                            Currently active emitters for n_eval frames, up to k_max emitters.
                            
        ~ failed_updates :  array, [n_eval]
                            Inverted failed updates returns an array of frames with a successful jump.
                            
        ll_new :            array, [n_eval]
                            New log Likelihood value, belonging to updated parameters.
                            
        prior_new :         array, [n_eval]
                            Previous prior probability value, belonging to current parameters.

        """
        n_eval, km, numFrames = I.shape
        
        # find frames with more than 1 active emitter
        modelParams = emitterState.sum(1)
        loc = np.where(modelParams > 1)[0]
        
        # if no frames qualify, return current parameters
        if len(loc) == 0:
            return pos, I, backgroundParams, emitterState, np.zeros(n_eval, dtype=bool), ll_prev, prior_prev

        # in each frame, pick an emitter to update
        update_indices = np.zeros(len(loc), dtype = int)
        for i in range(len(loc)):
            if emitterState[i].sum() != 0:
                update_indices[i] = np.random.choice(np.arange(k_max)[emitterState[i]])     
            else:
                update_indices[i] = 0
        
        # find a group of emitters within clustering radius of the selected emitter
        Closest = ClosestMulti2(pos, emitterState, loc, update_indices, 2*self.sigma_PSF, z_rescale = self.z_rescale)

        # indexing to update the correct frames and emitters
        a1 = np.where(Closest[loc])[0]
        a2 = np.where(Closest[loc])[1]
        moves = Closest[loc].sum()
        
        # update the positions of the emitters
        pos_update = np.copy(pos)
        pos_update[a1,a2,0] += np.random.normal(0, self.sigma_x, moves)
        pos_update[a1,a2,1] += np.random.normal(0, self.sigma_y, moves)
        pos_update[a1,a2,2] += np.random.normal(0, self.sigma_z, moves)
        
        # moving I over all frames randomly
        I_update = np.copy(I)
        I_update[a1,a2,:] += np.random.normal(0, self.sigma_I, [moves, numFrames])

        # calculate posterior ratio
        prior_new = self.prior_func.calc(pos_update, I_update, backgroundParams, emitterState, smpIndices)
        ll_new = self.LL_func.Likelihood(pos_update, I_update*emitterState[:,:,None], backgroundParams, smpIndices)

        PR = np.exp(prior_new + ll_new - prior_prev - ll_prev)
        
        # random number used for accepting/rejecting jumps
        u = np.random.uniform(0,1,n_eval)

        # Accept jumps where acceptance ratio > u with u ~ U(0,1)
        # update the estimates
        failed_updates = (PR < u) + np.isnan(PR)        # where posterior ratio < random number AND where posterior ratio isNaN
        pos_update[failed_updates] = pos[failed_updates]        # rejected jumps in each dataset get set back to their original values
        I_update[failed_updates] = I[failed_updates]
        ll_new[failed_updates] = ll_prev[failed_updates]
        prior_new[failed_updates] = prior_prev[failed_updates]
        
        return pos_update, I_update, backgroundParams, emitterState, ~failed_updates, ll_new, prior_new  
  
         
class Background_Move:
    """
    The background move updates the background photon count with a sample from
    a Gaussian distribution. Initially, background photon intensity was modeled 
    as a tilted plane. State of the art has since returned to using either a 
    uniform background photon count or per-pixel expectations based on setup 
    experiments.
    """
    def __init__(self, sigma_bg, sigma_ax, sigma_ay, prior_func, LL_func):
        self.sigma_bg = sigma_bg                    # standard deviation for sampling background photon count offset
        self.sigma_ax = sigma_ax                    # OBSOLETE standard deviation for sampling background photon count x-plane tilt
        self.sigma_ay = sigma_ay                    # OBSOLETE standard deviation for sampling background photon count x-plane tilt
        self.prior_func = prior_func                # prior information
        self.LL_func = LL_func                      # Likelihood function
        
    def calc(self, pos, I, backgroundParams, emitterState, k_max, smpIndices, ll_prev, prior_prev):
        """
        Executing this function generates new parameters and accepts or rejects
        them. 

        Parameters
        ----------
        pos :               array, [n_eval, k_max, dim]
                            Current position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                       
        I :                 array, [n_eval, k_max, numFrames]
                            Current emitter intensity for n_eval frames, up to k_max emitters, with numFrames shot in parallel.
                            
        backgroundParams :  array, [n_eval]
                            Current background photon count for n_eval frames. 
            
        emitterState :      array, [n_eval, k_max]
                            Currently active emitters for n_eval frames, up to k_max emitters.
                        
        k_max :             float
                            Maximum amount of active emitters.
                            
        smpIndices          array, [n_eval]
                            Informs Likelihood function which frames to use to calculate the Likelihood given the parameters.
                            
        ll_prev :           array, [n_eval]
                            Previous log Likelihood value, belonging to current parameters.
                            
        prior_prev :        array, [n_eval]
                            Previous prior probability value, belonging to current parameters.

        Returns
        -------
        pos_update :        array, [n_eval, k_max, dim]
                            Updated position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                        
        I_update :          array, [n_eval, k_max, numFrames]
                            Updated emitter intensity for n_eval frames, up to k_max emitters, with numFrames shot in parallel.
                            
        backgroundParams :  array, [n_eval]
                            Current background photon count for n_eval frames.
                            
        emitterState :      array, [n_eval, k_max]
                            Currently active emitters for n_eval frames, up to k_max emitters.
                            
        ~ failed_updates :  array, [n_eval]
                            Inverted failed updates returns an array of frames with a successful jump.
                            
        ll_new :            array, [n_eval]
                            New log Likelihood value, belonging to updated parameters.
                            
        prior_new :         array, [n_eval]
                            Previous prior probability value, belonging to current parameters.

        """
        n_eval = emitterState.shape[0]
        
        # jump background parameters
        backgroundParams_update = np.copy(backgroundParams) + np.random.normal(0, self.sigma_bg, n_eval)
        
        # calculate posterior ratio
        prior_new = self.prior_func.calc(pos, I, backgroundParams_update, emitterState, smpIndices)
        ll_new = self.LL_func.Likelihood(pos, I*emitterState[:,:,None], backgroundParams_update, smpIndices)

        PR = np.exp(prior_new + ll_new - prior_prev - ll_prev)
        
        # random number for accepting/rejecting jumps
        u = np.random.uniform(0,1,n_eval)
        
        # Accept jumps where acceptance ratio > u with u ~ U(0,1)
        # update the estimates
        failed_updates = (PR < u) + np.isnan(PR)        # where posterior ratio < random number AND where posterior ratio isNaN
        backgroundParams_update[failed_updates] = backgroundParams[failed_updates]
        ll_new[failed_updates] = ll_prev[failed_updates]
        prior_new[failed_updates] = prior_prev[failed_updates]
        
        return pos, I, backgroundParams_update, emitterState, ~failed_updates, ll_new, prior_new
  
        
class Split:
    """
    The split move randomly selects an active emitter and splits it in two, 
    redistributing the photon intensity randomly while finding emitter 
    positions that conserve the center of mass. Pairs with merge.
    """
    def __init__(self, sigma_PSF, prior_func, LL_func, p_split, p_merge, z_rescale = 1):
        self.sigma_PSF = sigma_PSF                  # PSF width assuming Gaussian or astigmatic PSFs. approximate/tune otherwise.
        self.prior_func = prior_func                # prior information
        self.LL_func = LL_func                      # Likelihood function
        self.p_merge = p_merge                      # probability of selecting merge from the split/merge pair
        self.p_split = p_split                      # probability of selecting split from the split/merge pair
        self.z_rescale = z_rescale                  # flatten/stretch clustering sphere in z direction
        
    def calc(self, pos, I, backgroundParams, emitterState, k_max, smpIndices, ll_prev, prior_prev):
        """
        Executing this function generates a new model and parameters and 
        accepts or rejects them. 

        Parameters
        ----------
        pos :               array, [n_eval, k_max, dim]
                            Current position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                       
        I :                 array, [n_eval, k_max, numFrames]
                            Current emitter intensity for n_eval frames, up to k_max emitters, with numFrames shot in parallel.
                            
        backgroundParams :  array, [n_eval]
                            Current background photon count for n_eval frames. 
            
        emitterState :      array, [n_eval, k_max]
                            Currently active emitters for n_eval frames, up to k_max emitters.
                        
        k_max :             float
                            Maximum amount of active emitters.
                            
        smpIndices          array, [n_eval]
                            Informs Likelihood function which frames to use to calculate the Likelihood given the parameters.
                            
        ll_prev :           array, [n_eval]
                            Previous log Likelihood value, belonging to current parameters.
                            
        prior_prev :        array, [n_eval]
                            Previous prior probability value, belonging to current parameters.

        Returns
        -------
        pos_update :        array, [n_eval, k_max, dim]
                            Updated position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                        
        I_update :          array, [n_eval, k_max, numFrames]
                            Updated emitter intensity for n_eval frames, up to k_max emitters, with numFrames shot in parallel.
                            
        backgroundParams :  array, [n_eval]
                            Current background photon count for n_eval frames.
                            
        emitterState :      array, [n_eval, k_max]
                            Currently active emitters for n_eval frames, up to k_max emitters.
                            
        ~ failed_updates :  array, [n_eval]
                            Inverted failed updates returns an array of frames with a successful jump.
                            
        ll_new :            array, [n_eval]
                            New log Likelihood value, belonging to updated parameters.
                            
        prior_new :         array, [n_eval]
                            Previous prior probability value, belonging to current parameters.

        """
        modelParams = emitterState.sum(1)
        n_eval, km, numFrames = I.shape
        
        # return current model and parameters if no estimates are eligible for splitting
        splits = len(modelParams[modelParams < k_max])
        if splits == 0:
            return pos, I, backgroundParams, emitterState, np.zeros(n_eval, dtype=bool), ll_prev, prior_prev #, 'split failed: all sets are at k_max'
        
        # splitable frames
        loc = np.where(modelParams < k_max)[0]  
        
        # random numbers for split:
        u1 = np.random.beta(1,1, splits)
        u2 = np.random.normal(0, self.sigma_PSF, splits)         
        u3 = np.random.normal(0, self.sigma_PSF, splits)
        u4 = np.random.normal(0, self.sigma_PSF*self.z_rescale, splits)        
        
        
        # pick an active emitter to split
        j1 = np.zeros(len(loc), dtype = int)
        j2 = np.zeros(len(loc), dtype = int)
        emitterState_update = np.copy(emitterState)
        for i in range(len(loc)):
            if emitterState[loc[i]].sum() != 0:
                j1[i] = np.random.choice(np.arange(0, k_max)[emitterState[loc[i]]])     # emitter to split
                j2[i] = np.arange(0, k_max)[~emitterState[loc[i]]][0]                   # empty emitterState to move the newly generated emitter into
                emitterState_update[loc[i],j2[i]] = True
            else:
                j1[i] = 0
                j2[i] = 1
                
        # prepare position and intensity updates
        pos_update = np.copy(pos)
        I_update = np.copy(I)
        
        # split a single element in each sample
        # I_j1 = Ij * u1
        # I_j2 = Ij * (1 - u1)
        # multi-frame: each frame separately splits its intensity as normal
        I_update[loc, j1, :] = I[loc, j1, :] * u1[:,None]
        I_update[loc, j2, :] = I[loc, j1, :] * (1 - u1)[:,None]
        
        # x_j1 = x_j + u2
        # x_j2 = x_j - (u1 * u2) / (1 - u1)
        pos_update[loc,j1,0] += u2
        pos_update[loc,j2,0] = pos[loc,j1,0] - (u1 * u2) / (1 - u1)
        
        # y_j1 = y_j + u3
        # y_j2 = y_j - (u1 * u3) / (1 - u1)
        pos_update[loc,j1,1] += u3
        pos_update[loc,j2,1] = pos[loc,j1,1] - (u1 * u3) / (1 - u1)
        
        # 3d: z-positions conserving COM
        pos_update[loc,j1,2] += u4
        pos_update[loc,j2,2] = pos[loc,j1,2] - (u1 * u4) / (1 - u1)

        # calculate the acceptance rate
        prior_new = self.prior_func.calc(pos_update, I_update, backgroundParams, emitterState_update, smpIndices)
        ll_new = self.LL_func.Likelihood(pos_update, I_update*emitterState_update[:,:,None], backgroundParams, smpIndices)
        
        # posterior ratio
        PR = np.exp(prior_new + ll_new - prior_prev - ll_prev)/(self.prior_func.roisize + 2*self.prior_func.border)**2*(self.prior_func.mu_k/emitterState_update.sum(1))
        
        # joint probability of drawing u1, u2, u3, u4
        q = np.zeros(n_eval)
        q[loc] = gauss1d(u2, 0, self.sigma_PSF) * gauss1d(u3, 0, self.sigma_PSF) * gauss1d(u4, 0, self.sigma_PSF*self.z_rescale) * beta.pdf(u1,1,1)

        # determinant of the jacbobian of parameter update theta' to parameters and random numbers [theta, u1, u2, u3, u4] 
        det_jac = np.zeros([n_eval, numFrames])
        det_jac[loc, :] = I[loc, j1, :] / ((1 - u1)**3)[:,None]
        
        # acceptance ratio = Posterior ratio * interpair selection probability ratio / P(u1, u2, u3, u4) * determinant of jacobian \frac{\partial \theta'}{\partial \[\theta, u_1, u_2, u_3, u_4]} 
        A = np.zeros(n_eval)
        A[loc] = PR[loc] * self.p_merge/self.p_split / q[loc] * det_jac.prod(1)[loc]        

        # random number for accepting/rejecting the jumps
        u = np.random.uniform(0,1,n_eval)

        # Accept jumps where acceptance ratio > u with u ~ U(0,1)
        # update the estimates and model
        failed_updates = (A < u) + np.isnan(A)        # where posterior ratio < random number AND where posterior ratio isNaN
        pos_update[failed_updates] = pos[failed_updates]
        I_update[failed_updates] = I[failed_updates]
        emitterState_update[failed_updates] = emitterState[failed_updates]
        ll_new[failed_updates] = ll_prev[failed_updates]
        prior_new[failed_updates] = prior_prev[failed_updates]
        
        return pos_update, I_update, backgroundParams, emitterState_update, ~failed_updates, ll_new, prior_new
    
    
class Generalized_Split:
    """
    Generalized split finds a cluster of N emitters and splits them into N+1
    emitters, redistributing the photon intensity randomly while finding 
    emitter positions that conserve the center of mass. Pairs with generalized
    merge.
    """
    def __init__(self, sigma_PSF, prior_func, LL_func, p_split, p_merge, z_rescale = 1):
        self.sigma_PSF = sigma_PSF                  # PSF width assuming Gaussian or astigmatic PSFs. approximate/tune otherwise.
        self.prior_func = prior_func                # prior information
        self.LL_func = LL_func                      # Likelihood function
        self.p_merge = p_merge                      # probability of selecting merge from the split/merge pair
        self.p_split = p_split                      # probability of selecting split from the split/merge pair
        self.z_rescale = z_rescale                  # flatten/stretch clustering sphere in z direction
        
    def calc(self, pos, I, backgroundParams, emitterState, k_max, smpIndices, ll_prev, prior_prev):
        """
        Executing this function generates a new model and parameters and 
        accepts or rejects them. 

        Parameters
        ----------
        pos :               array, [n_eval, k_max, dim]
                            Current position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                       
        I :                 array, [n_eval, k_max, numFrames]
                            Current emitter intensity for n_eval frames, up to k_max emitters, with numFrames shot in parallel.
                            
        backgroundParams :  array, [n_eval]
                            Current background photon count for n_eval frames. 
            
        emitterState :      array, [n_eval, k_max]
                            Currently active emitters for n_eval frames, up to k_max emitters.
                        
        k_max :             float
                            Maximum amount of active emitters.
                            
        smpIndices          array, [n_eval]
                            Informs Likelihood function which frames to use to calculate the Likelihood given the parameters.
                            
        ll_prev :           array, [n_eval]
                            Previous log Likelihood value, belonging to current parameters.
                            
        prior_prev :        array, [n_eval]
                            Previous prior probability value, belonging to current parameters.

        Returns
        -------
        pos_update :        array, [n_eval, k_max, dim]
                            Updated position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                        
        I_update :          array, [n_eval, k_max, numFrames]
                            Updated emitter intensity for n_eval frames, up to k_max emitters, with numFrames shot in parallel.
                            
        backgroundParams :  array, [n_eval]
                            Current background photon count for n_eval frames.
                            
        emitterState :      array, [n_eval, k_max]
                            Currently active emitters for n_eval frames, up to k_max emitters.
                            
        ~ failed_updates :  array, [n_eval]
                            Inverted failed updates returns an array of frames with a successful jump.
                            
        ll_new :            array, [n_eval]
                            New log Likelihood value, belonging to updated parameters.
                            
        prior_new :         array, [n_eval]
                            Previous prior probability value, belonging to current parameters.

        """
        modelParams = emitterState.sum(1)
        n_eval, km, numFrames = I.shape
        
        # find eligible frames
        delete = np.array([], dtype = int)
        loc = np.where(modelParams < k_max)[0]          # sets with less than maxEmitters
        for i in np.arange(len(loc)):
            if modelParams[loc[i]] < 2:
                delete = np.append(delete, i)           # sets with less than 2 active emitters are not viable for g-split
        loc = np.delete(loc, delete)  
        
        # return current model and parameters if no frames are eligible for generalized split
        if len(loc) == 0:
            return pos, I, backgroundParams, emitterState, np.zeros(n_eval, dtype=bool), ll_prev, prior_prev 

        # pick an emitter to split
        j1 = np.zeros(len(loc), dtype = int)
        j2 = np.zeros(len(loc), dtype = int)
        emitterState_update = np.copy(emitterState)

        for i in range(len(loc)):
            if emitterState[loc[i]].sum() != 0:
                j1[i] = np.random.choice(np.arange(0, k_max)[emitterState[loc[i]]])
                j2[i] = np.arange(0, k_max)[~emitterState[loc[i]]][0]
                emitterState_update[loc[i],j2[i]] = True
            else:
                j1[i] = 0
                j2[i] = 1

        # find a cluster of emitters centered around emitter j1
        Closest = ClosestMulti2(pos, emitterState, loc, j1, 2*self.sigma_PSF, z_rescale = self.z_rescale)
        Np = Closest.sum(1)

        # exclude frames with cluster size < 2 
        delete = np.array([], dtype = int)
        for i in np.arange(len(loc)):
            if Np[i] <= 1:                              # no emitters close to j1, only one element in the cluster
                delete = np.append(delete, i)
                
        emitterState_update[loc[delete], j2[delete]] = False
        loc = np.delete(loc, delete)

        j1 = np.delete(j1, delete)
        j2 = np.delete(j2, delete)

        # random numbers for split:
        u1 = np.random.beta(1,1, len(loc))
        u2 = np.random.normal(0, self.sigma_PSF, len(loc))         
        u3 = np.random.normal(0, self.sigma_PSF, len(loc))
        u4 = np.random.normal(0, self.sigma_PSF*self.z_rescale, len(loc))

        # initialize position and intensity updates
        pos_update = np.copy(pos)
        pos_temp = np.copy(pos)
        I_update = np.copy(I)
        I_temp = np.copy(I)
        
        # split a single element from the groups in each sample
        # rescale emitters in group with (1 - u1)
        I_temp[loc,:,:] = I[loc,:,:] * Closest[loc,:,None] * (1 - u1[:,None, None])

        # generate new emitter with intensity u1 * sum(I_closest)
        I_tot = (I[loc,:,:]*Closest[loc,:,None]).sum(1)
        I_update[loc,j2,:] = I_tot * u1[:,None]


        # x_i' = (x_i - u1(1/N*sum_1^N (x_i + u2))) / (1 - u1)        
        pos_temp[loc,:,0] = (pos[loc,:,0]*Closest[loc] - (((pos[loc,:,0] + u2[:,None])*Closest[loc]).sum(1)/Np[loc] * u1)[:,None]) / (1 - u1[:,None])
        pos_update[loc,j2,0] = ((pos[loc,:,0] + u2[:,None])*Closest[loc]).sum(1)/Np[loc]
        
        # y
        pos_temp[loc,:,1] = (pos[loc,:,1]*Closest[loc] - (((pos[loc,:,1] + u3[:,None])*Closest[loc]).sum(1)/Np[loc] * u1)[:,None]) / (1 - u1[:,None])
        pos_update[loc,j2,1] = ((pos[loc,:,1] + u3[:,None])*Closest[loc]).sum(1)/Np[loc]
        
        # z
        pos_temp[loc,:,2] = (pos[loc,:,2]*Closest[loc] - (((pos[loc,:,2] + u4[:,None])*Closest[loc]).sum(1)/Np[loc] * u1)[:,None]) / (1 - u1[:,None])
        pos_update[loc,j2,2] = ((pos[loc,:,2] + u4[:,None])*Closest[loc]).sum(1)/Np[loc]
        
        # indexing for repositioned emitters in the cluster
        a1 = np.where(Closest[loc])[0]
        a2 = np.where(Closest[loc])[1]
        
        # update the emitter intensities and positions
        I_update[a1,a2,:] = I_temp[a1,a2,:]
        pos_update[a1, a2, 0] = pos_temp[a1, a2, 0]
        pos_update[a1, a2, 1] = pos_temp[a1, a2, 1]
        pos_update[a1, a2, 2] = pos_temp[a1, a2, 2]
        
        # calculate the acceptance rate
        prior_new = self.prior_func.calc(pos_update, I_update, backgroundParams, emitterState_update, smpIndices)
        ll_new = self.LL_func.Likelihood(pos_update, I_update*emitterState_update[:,:,None], backgroundParams, smpIndices)

        # posterior ratio
        PR = np.exp(prior_new + ll_new - prior_prev - ll_prev)/(self.prior_func.roisize + 2*self.prior_func.border)**2*(self.prior_func.mu_k/emitterState_update.sum(1))
        
        # joint probability of drawing u1, u2, u3, u4
        q = np.zeros(n_eval)
        q[loc] = gauss1d(u2, 0, self.sigma_PSF) * gauss1d(u3, 0, self.sigma_PSF) * gauss1d(u4, 0, self.sigma_PSF*self.z_rescale) * beta.pdf(u1,1,1)

        # determinant of the jacbobian of parameter update theta' to parameters and random numbers [theta, u1, u2, u3, u4] 
        det_jac = np.zeros([n_eval, numFrames])
        det_jac[loc, :] = I_tot / ((1 - u1)**(Np[loc]*2 + 1))[:,None]
        
        # acceptance ratio = Posterior ratio * interpair selection probability ratio / P(u1, u2, u3, u4) * determinant of jacobian \frac{\partial \theta'}{\partial \[\theta, u_1, u_2, u_3, u_4]} 
        A = np.zeros(n_eval)
        A[loc] = PR[loc] * self.p_merge/self.p_split / q[loc] * det_jac.prod(1)[loc]
        
        # random number for accepting/rejecting the jumps
        u = np.random.uniform(0,1,n_eval)
        
        # Accept jumps where acceptance ratio > u with u ~ U(0,1)
        # update the estimates and model
        failed_updates = (A < u) + np.isnan(A)        # where posterior ratio < random number AND where posterior ratio isNaN
        pos_update[failed_updates] = pos[failed_updates]
        I_update[failed_updates] = I[failed_updates]
        emitterState_update[failed_updates] = emitterState[failed_updates]
        ll_new[failed_updates] = ll_prev[failed_updates]
        prior_new[failed_updates] = prior_prev[failed_updates]
        
        return pos_update, I_update, backgroundParams, emitterState_update, ~failed_updates, ll_new, prior_new
   
    
class Merge:
    """
    The merge move randomly selects two active emitters and combines them into 
    one, retaining total emitter intensity and center of mass. Pairs with 
    split.
    """
    def __init__(self, sigma_PSF, prior_func, LL_func, p_merge, p_split, z_rescale = 1):
        self.sigma_PSF = sigma_PSF                  # PSF width assuming Gaussian or astigmatic PSFs. approximate/tune otherwise.
        self.prior_func = prior_func                # prior information
        self.LL_func = LL_func                      # Likelihood function
        self.p_merge = p_merge                      # probability of selecting merge from the split/merge pair
        self.p_split = p_split                      # probability of selecting split from the split/merge pair
        self.z_rescale = z_rescale                  # flatten/stretch clustering sphere in z direction
        
    def calc(self, pos, I, backgroundParams, emitterState, k_max, smpIndices, ll_prev, prior_prev):
        """
        Executing this function generates a new model and parameters and 
        accepts or rejects them. 
        
        Parameters
        ----------
        pos :               array, [n_eval, k_max, dim]
                            Current position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                       
        I :                 array, [n_eval, k_max, numFrames]
                            Current emitter intensity for n_eval frames, up to k_max emitters, with numFrames shot in parallel.
                            
        backgroundParams :  array, [n_eval]
                            Current background photon count for n_eval frames. 
            
        emitterState :      array, [n_eval, k_max]
                            Currently active emitters for n_eval frames, up to k_max emitters.
                        
        k_max :             float
                            Maximum amount of active emitters.
                            
        smpIndices          array, [n_eval]
                            Informs Likelihood function which frames to use to calculate the Likelihood given the parameters.
                            
        ll_prev :           array, [n_eval]
                            Previous log Likelihood value, belonging to current parameters.
                            
        prior_prev :        array, [n_eval]
                            Previous prior probability value, belonging to current parameters.
        
        Returns
        -------
        pos_update :        array, [n_eval, k_max, dim]
                            Updated position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                        
        I_update :          array, [n_eval, k_max, numFrames]
                            Updated emitter intensity for n_eval frames, up to k_max emitters, with numFrames shot in parallel.
                            
        backgroundParams :  array, [n_eval]
                            Current background photon count for n_eval frames.
                            
        emitterState :      array, [n_eval, k_max]
                            Currently active emitters for n_eval frames, up to k_max emitters.
                            
        ~ failed_updates :  array, [n_eval]
                            Inverted failed updates returns an array of frames with a successful jump.
                            
        ll_new :            array, [n_eval]
                            New log Likelihood value, belonging to updated parameters.
                            
        prior_new :         array, [n_eval]
                            Previous prior probability value, belonging to current parameters.
        
        """
        modelParams = emitterState.sum(1)
        n_eval, km, numFrames = I.shape
        
        # return current model and parameters if no estimates are eligible for merging
        merges = len(modelParams[modelParams >= 2])
        if merges == 0:
            return pos, I, backgroundParams, emitterState, np.zeros(n_eval, dtype=bool), ll_prev, prior_prev
        
        # mergable frames
        loc = np.where(modelParams >= 2)[0]         
       
        # pick two emitters to merge
        j1 = np.zeros(len(loc), dtype = int)
        j2 = np.zeros(len(loc), dtype = int)
        emitterState_update = np.copy(emitterState)
        for i in range(len(loc)):
            if emitterState[loc[i]].sum() != 0:
                j2[i] = np.random.choice(np.arange(0, k_max)[emitterState[loc[i]]])     # pick emitter to merge, index j2 turns off
                emitterState_update[loc[i],j2[i]] = False
                j1[i] = np.random.choice(np.arange(0, k_max)[emitterState_update[loc[i]]])     # pick another emitter to merge
            else:
                j2[i] = 0
                j1[i] = 1
                
        # intialize position and intensity updates
        pos_update = np.copy(pos)
        I_update = np.copy(I)
        
        # read eligible emitter xyz position and intensity
        # I_j = I_j1 + I_j2
        Ij1 = I[loc, j1, :]
        Ij2 = I[loc, j2, :]
        
        I_update[loc, j1, :] = Ij1 + Ij2
        I_update[loc, j2, :] = 0

        xj1 = pos[loc, j1, 0]
        xj2 = pos[loc, j2, 0]
        yj1 = pos[loc, j1, 1]
        yj2 = pos[loc, j2, 1]
        zj1 = pos[loc, j1, 2]
        zj2 = pos[loc, j2, 2]
        
        # merge two elements in each sample
        # x_j = (I_j1 * x_j1 + I_j2 * x_j2) / I_j
        # multi-frame: for CoM calculation, sum intensities of all frames for 
        # corresponding emitters and multiply with position, divide by total
        # intensity over all frames
        pos_update[loc, j1, 0] = (Ij1.sum(1) * xj1 + Ij2.sum(1) * xj2) / I_update[loc, j1, :].sum(1)
        pos_update[loc, j2, 0] = 0
        
        pos_update[loc, j1, 1] = (Ij1.sum(1) * yj1 + Ij2.sum(1) * yj2) / I_update[loc, j1, :].sum(1)
        pos_update[loc, j2, 1] = 0
        
        pos_update[loc, j1, 2] = (Ij1.sum(1) * zj1 + Ij2.sum(1) * zj2) / I_update[loc, j1, :].sum(1)
        pos_update[loc, j2, 2] = 0
        
        # random number samples for merge -> determine from position and intensity update
        u1 = Ij1.sum(1) / I_update[loc, j1, :].sum(1)
        u2 = xj1 - pos_update[loc, j1, 0]
        u3 = yj1 - pos_update[loc, j1, 1]
        u4 = zj1 - pos_update[loc, j1, 2]
        
        
        # calculate the acceptance rate
        prior_new = self.prior_func.calc(pos_update, I_update, backgroundParams, emitterState_update, smpIndices)
        ll_new = self.LL_func.Likelihood(pos_update, I_update*emitterState_update[:,:,None], backgroundParams, smpIndices)

        # posterior ratio
        PR = np.exp(prior_new + ll_new - prior_prev - ll_prev)*(self.prior_func.roisize + 2*self.prior_func.border)**2/(self.prior_func.mu_k/emitterState_update.sum(1))
        
        # joint probability of drawing u1, u2, u3, u4
        q = np.zeros(n_eval)
        q[loc] = gauss1d(u2, 0, self.sigma_PSF) * gauss1d(u3, 0, self.sigma_PSF) * gauss1d(u4, 0, self.sigma_PSF*self.z_rescale) * beta.pdf(u1,1,1)
        
        # determinant of the jacbobian of parameter update theta' to parameters and random numbers [theta, u1, u2, u3, u4] 
        # I_j / (1 - u1)**2
        det_jac = np.zeros(n_eval)
        det_jac[loc] = (I_update[loc, j1, :] / ((1 - u1)**3)[:,None]).prod(1)       # multiplying probabilities over all frames
        
        # acceptance ratio = Posterior ratio * interpair selection probability ratio * P(u1, u2, u3, u4) / determinant of jacobian \frac{\partial \theta'}{\partial \[\theta, u_1, u_2, u_3, u_4]} 
        A = np.zeros(n_eval)
        A[loc] = PR[loc] * self.p_split/self.p_merge * q[loc] / det_jac[loc]
        
        # random number for accepting/rejecting the jumps
        u = np.random.uniform(0,1,n_eval)

        # Accept jumps where acceptance ratio > u with u ~ U(0,1)
        # update the estimates and model       
        failed_updates = (A < u) + np.isnan(A)        # where posterior ratio < random number AND where posterior ratio isNaN
        pos_update[failed_updates] = pos[failed_updates]
        I_update[failed_updates] = I[failed_updates]
        emitterState_update[failed_updates] = emitterState[failed_updates]
        ll_new[failed_updates] = ll_prev[failed_updates]
        prior_new[failed_updates] = prior_prev[failed_updates]
        
        return pos_update, I_update, backgroundParams, emitterState_update, ~failed_updates, ll_new, prior_new
   
        
class Generalized_Merge:
    """
    Generalized merge finds a cluster of N + 1 emitters and merges them into N
    emitters, redistributing the photon intensity randomly while finding 
    emitter positions that conserve the center of mass. Pairs with generalized
    split.
    """
    def __init__(self, sigma_PSF, prior_func, LL_func, p_merge, p_split, z_rescale = 1):
        self.sigma_PSF = sigma_PSF                  # PSF width assuming Gaussian or astigmatic PSFs. approximate/tune otherwise.
        self.prior_func = prior_func                # prior information
        self.LL_func = LL_func                      # Likelihood function
        self.p_merge = p_merge                      # probability of selecting merge from the split/merge pair
        self.p_split = p_split                      # probability of selecting split from the split/merge pair
        self.z_rescale = z_rescale                  # flatten/stretch clustering sphere in z direction
        
    def calc(self, pos, I, backgroundParams, emitterState, k_max, smpIndices, ll_prev, prior_prev):
        """
        Executing this function generates a new model and parameters and 
        accepts or rejects them. 
        
        Parameters
        ----------
        pos :               array, [n_eval, k_max, dim]
                            Current position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                       
        I :                 array, [n_eval, k_max, numFrames]
                            Current emitter intensity for n_eval frames, up to k_max emitters, with numFrames shot in parallel.
                            
        backgroundParams :  array, [n_eval]
                            Current background photon count for n_eval frames. 
            
        emitterState :      array, [n_eval, k_max]
                            Currently active emitters for n_eval frames, up to k_max emitters.
                        
        k_max :             float
                            Maximum amount of active emitters.
                            
        smpIndices          array, [n_eval]
                            Informs Likelihood function which frames to use to calculate the Likelihood given the parameters.
                            
        ll_prev :           array, [n_eval]
                            Previous log Likelihood value, belonging to current parameters.
                            
        prior_prev :        array, [n_eval]
                            Previous prior probability value, belonging to current parameters.
        
        Returns
        -------
        pos_update :        array, [n_eval, k_max, dim]
                            Updated position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                        
        I_update :          array, [n_eval, k_max, numFrames]
                            Updated emitter intensity for n_eval frames, up to k_max emitters, with numFrames shot in parallel.
                            
        backgroundParams :  array, [n_eval]
                            Current background photon count for n_eval frames.
                            
        emitterState :      array, [n_eval, k_max]
                            Currently active emitters for n_eval frames, up to k_max emitters.
                            
        ~ failed_updates :  array, [n_eval]
                            Inverted failed updates returns an array of frames with a successful jump.
                            
        ll_new :            array, [n_eval]
                            New log Likelihood value, belonging to updated parameters.
                            
        prior_new :         array, [n_eval]
                            Previous prior probability value, belonging to current parameters.
        
        """
        modelParams = emitterState.sum(1)
        n_eval = len(modelParams)

        # return current model and parameters if no frames are eligible for generalized merge
        loc = np.where(modelParams >= 3)[0]         # sets need at least 3 emitters for g-merge
        if len(loc) == 0:
            return pos, I, backgroundParams, emitterState, np.zeros(n_eval, dtype=bool), ll_prev, prior_prev 

        # pick an emitter to merge
        j2 = np.zeros(len(loc), dtype = int)
        emitterState_update = np.copy(emitterState)

        for i in range(len(loc)):
            if emitterState[loc[i]].sum() != 0:
                j2[i] = np.random.choice(np.arange(0, k_max)[emitterState[loc[i]]])     # merge j2, turns off
                emitterState_update[loc[i],j2[i]] = False
            else:
                j2[i] = 0
        
        # find a cluster of emitters centered around emitter j1
        Closest = ClosestMulti2(pos, emitterState, loc, j2, 3*self.sigma_PSF, z_rescale = self.z_rescale)    #  3*sigma or 2*sigma?
        Np = Closest.sum(1)
        
        # exclude frames with cluster size < 2
        delete = np.array([], dtype = int)
        for i in range(len(loc)):
            if Np[i] <= 1:                              # no emitters close to j2, only one element in the cluster
                delete = np.append(delete, i)
 
        emitterState_update[loc[delete], j2[delete]] = True
        loc = np.delete(loc, delete)
        j2 = np.delete(j2, delete)
        
        # initialize position and intensity updates
        pos_update = np.copy(pos)
        pos_temp = np.copy(pos)
        I_update = np.copy(I)
        I_temp = np.copy(I)
        
        # merge elements in each sample
        # determine random sample for intensity parameter
        u1 = I[loc,j2,:].sum(1) / (I[loc,:,:]*Closest[loc,:,None]).sum(1).sum(1)   # combined intensity of all em in cluster

        # merge intensity
        I_temp[loc, :,:] /= (1 - u1)[:,None,None]          
        Closest[loc,j2] = False

        # merge x, y, z, determine random sample for x, y, and z position
        # x
        pos_temp[loc,:,0] = u1[:,None] * pos[loc,j2,0][:,None] + (1 - u1)[:,None] * pos[loc,:,0]
        u2 = pos[loc,j2,0] - (1/(Np[loc] - 1))*(pos_temp[loc,:,0]*Closest[loc]).sum(1)

        # y
        pos_temp[loc,:,1] = u1[:,None] * pos[loc,j2,1][:,None] + (1 - u1)[:,None] * pos[loc,:,1]
        u3 = pos[loc,j2,1] - (1/(Np[loc] - 1))*(pos_temp[loc,:,1]*Closest[loc]).sum(1)

        # z
        pos_temp[loc,:,2] = u1[:,None] * pos[loc,j2,2][:,None] + (1 - u1)[:,None] * pos[loc,:,2]
        u4 = pos[loc,j2,2] - (1/(Np[loc] - 1))*(pos_temp[loc,:,2]*Closest[loc]).sum(1)
        
        # update intensity and position
        a1, a2 = np.where(Closest[loc])
        I_update[a1, a2, :] = I_temp[a1, a2, :]
        pos_update[a1, a2, 0] = pos_temp[a1, a2, 0]
        pos_update[a1, a2, 1] = pos_temp[a1, a2, 1]       
        
        # set params to 0 for disappearing emitter        
        pos_update[loc,j2] = 0
        I_update[loc,j2,:] = 0
        
        # calculate the acceptance rate
        prior_new = self.prior_func.calc(pos_update, I_update, backgroundParams, emitterState_update, smpIndices)
        ll_new = self.LL_func.Likelihood(pos_update, I_update*emitterState_update[:,:,None], backgroundParams, smpIndices)

        # posterior ratio
        PR = np.exp(prior_new + ll_new - prior_prev - ll_prev)*(self.prior_func.roisize + 2*self.prior_func.border)**2/(self.prior_func.mu_k/emitterState_update.sum(1))
        
        # joint probability of drawing u1, u2, u3, u4
        q = np.zeros(n_eval)
        q[loc] = gauss1d(u2, 0, self.sigma_PSF) * gauss1d(u3, 0, self.sigma_PSF) * gauss1d(u4, 0, self.sigma_PSF*self.z_rescale) * beta.pdf(u1,1,1)
        
        # determinant of the jacbobian of parameter update theta' to parameters and random numbers [theta, u1, u2, u3, u4] 
        I_tot = (I[loc,:,:]*Closest[loc,:,None]).sum(1)               # combined intensity of all except the emitter turning off
        det_jac = np.zeros(n_eval)
        det_jac[loc] = (I_tot / (( 1 - u1)**(Np[loc]*2 + 1))[:,None]).prod(1)
        
        # acceptance ratio = Posterior ratio * interpair selection probability ratio * P(u1, u2, u3, u4) / determinant of jacobian \frac{\partial \theta'}{\partial \[\theta, u_1, u_2, u_3, u_4]} 
        A = np.zeros(n_eval)
        A[loc] = PR[loc] * self.p_split/self.p_merge * q[loc] / det_jac[loc]
             
        # random number for accepting/rejecting the jumps
        u = np.random.uniform(0,1,n_eval)
        
        # Accept jumps where acceptance ratio > u with u ~ U(0,1)
        # update the estimates and model
        failed_updates = (A < u) + np.isnan(A)        # where posterior ratio < random number AND where posterior ratio isNaN
        pos_update[failed_updates] = pos[failed_updates]
        I_update[failed_updates] = I[failed_updates]
        emitterState_update[failed_updates] = emitterState[failed_updates]
        ll_new[failed_updates] = ll_prev[failed_updates]
        prior_new[failed_updates] = prior_prev[failed_updates]
        return pos_update, I_update, backgroundParams, emitterState_update, ~failed_updates, ll_new, prior_new


class Birth:
    """
    The birth move randomly generates a new emitter based on the residual 
    image. The residual image is found by subtracting the current expected
    value from the frame and normalizing to form a probability distribution.
    Pairs with death.
    """
    def __init__(self, sigma_PSF, prior_func, LL_func, p_birth, p_death, z_range):
        self.sigma_PSF = sigma_PSF                  # PSF width assuming Gaussian or astigmatic PSFs. approximate/tune otherwise.
        self.prior_func = prior_func                # prior information
        self.LL_func = LL_func                      # Likelihood function
        self.p_birth = p_birth                      # probability of selecting birth from the birth/death pair
        self.p_death = p_death                      # probability of selecting death from the birth/death pair
        self.z_range = z_range                      # array containing minimum and maximum axial position
        
    def residual_image(self, pos, I, backgroundParams, emitterState, loc):
        """
        Executing this function generates the residual image using the current 
        estimate and the measurement.

        Parameters
        ----------
        pos :               array, [n_eval, k_max, dim]
                            Current position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                       
        I :                 array, [n_eval, k_max, numFrames]
                            Current emitter intensity for n_eval frames, up to k_max emitters, with numFrames shot in parallel.
                            
        backgroundParams :  array, [n_eval]
                            Current background photon count for n_eval frames. 
            
        emitterState :      array, [n_eval, k_max]
                            Currently active emitters for n_eval frames, up to k_max emitters.
                            
        loc :               array, [n_eval]
                            Points to the frames corresponding to the estimates.

        Returns
        -------
        diff :              array, [n_eval, roisize, roisize]
                            Difference between the frame and the expected value for n_eval frames

        """
        
        # read the samples
        samples = self.LL_func.samples
        
        # calculate expected value from positions, intensities, and active emitters
        ev = self.LL_func.ExpectedValue(pos, I*emitterState[:,:,None])
        
        # residual image = sample - expected photon count from emitters - expected background photons
        diff = samples[loc] - ev - backgroundParams[:,None,None,None]  
        
        # no negative values
        diff[diff < 0] = 0
        
        return diff     
    
    def calc(self, pos, I, backgroundParams, emitterState, k_max, smpIndices, ll_prev, prior_prev):
        """
        Executing this function generates a new model and parameters and 
        accepts or rejects them. 
        
        Parameters
        ----------
        pos :               array, [n_eval, k_max, dim]
                            Current position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                       
        I :                 array, [n_eval, k_max, numFrames]
                            Current emitter intensity for n_eval frames, up to k_max emitters, with numFrames shot in parallel.
                            
        backgroundParams :  array, [n_eval]
                            Current background photon count for n_eval frames. 
            
        emitterState :      array, [n_eval, k_max]
                            Currently active emitters for n_eval frames, up to k_max emitters.
                        
        k_max :             float
                            Maximum amount of active emitters.
                            
        smpIndices          array, [n_eval]
                            Informs Likelihood function which frames to use to calculate the Likelihood given the parameters.
                            
        ll_prev :           array, [n_eval]
                            Previous log Likelihood value, belonging to current parameters.
                            
        prior_prev :        array, [n_eval]
                            Previous prior probability value, belonging to current parameters.
        
        Returns
        -------
        pos_update :        array, [n_eval, k_max, dim]
                            Updated position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                        
        I_update :          array, [n_eval, k_max, numFrames]
                            Updated emitter intensity for n_eval frames, up to k_max emitters, with numFrames shot in parallel.
                            
        backgroundParams :  array, [n_eval]
                            Current background photon count for n_eval frames.
                            
        emitterState :      array, [n_eval, k_max]
                            Currently active emitters for n_eval frames, up to k_max emitters.
                            
        ~ failed_updates :  array, [n_eval]
                            Inverted failed updates returns an array of frames with a successful jump.
                            
        ll_new :            array, [n_eval]
                            New log Likelihood value, belonging to updated parameters.
                            
        prior_new :         array, [n_eval]
                            Previous prior probability value, belonging to current parameters.
        
        """
        modelParams = emitterState.sum(1)
        n_eval, km, numFrames = I.shape
        
        # return current model and parameters if no estimates are eligible for birth
        births = len(modelParams[modelParams < k_max])
        if births == 0:
            return pos, I, backgroundParams, emitterState, np.zeros(n_eval, dtype=bool), ll_prev, prior_prev 
        
        # birthable frames
        loc = np.where(modelParams < k_max)[0]        
        j1 = np.zeros(len(loc), dtype = int)
        emitterState_update = np.copy(emitterState)
        
        # activate the first non-active emitter in emitterState
        for i in range(births):               # not i in loc
            j1[i] = np.arange(0, k_max)[~emitterState[loc[i]]][0]
            emitterState_update[loc[i], j1[i]] = True

        # construct residual image
        residual = self.residual_image(pos[loc], I[loc], backgroundParams[loc], emitterState[loc], loc)

        # select a pixel from the region of interest to generate a new emitter
        # multi-frame, take product of the frames to find birth probabilities
        residual = residual.prod(1)
        x_choice = np.zeros(births, dtype = int)
        y_choice = np.zeros(births, dtype = int)
        for i in range(births):    
            if residual[i,:,:].sum() == 0:
                residual[i,:,:] = 1
            
            x_choice[i] = np.random.choice(np.arange(np.size(residual,1)), p = residual[i,:,:].sum(1)/residual[i,:,:].sum())
            p2 = residual[i, x_choice[i], :]/residual[i,x_choice[i],:].sum()
            y_choice[i] = np.random.choice(np.arange(np.size(residual,2)), p = p2)
            
        # uniformly sample a position from within the pixel
        x_new = np.random.uniform(x_choice - .5, x_choice + .5)
        y_new = np.random.uniform(y_choice - .5, y_choice + .5)
        
        # axial position uniformly sampled over 3d psf range
        z_new = np.random.uniform(self.z_range[0], self.z_range[1], size = births)
        u4 = 1 / (self.z_range[1] - self.z_range[0])
        
        # initialize position and intensity updates
        pos_update = np.copy(pos)
        I_update = np.copy(I)
        
        # update position
        pos_update[loc,j1,0] = x_new
        pos_update[loc,j1,1] = y_new
        pos_update[loc,j1,2] = z_new
        
        # update intensity
        I_update[loc,j1,:] = np.random.choice(np.arange(len(self.prior_func.I_PDF)), size = [births, numFrames], p = self.prior_func.I_PDF)
        
        # calculate the acceptance rate
        prior_new = self.prior_func.calc(pos_update, I_update, backgroundParams, emitterState_update, smpIndices)
        ll_new = self.LL_func.Likelihood(pos_update, I_update*emitterState_update[:,:,None], backgroundParams, smpIndices)
        
        # posterior ratio
        PR = np.exp(ll_new - ll_prev + prior_new - prior_prev)

        # normalized residual
        res_norm = residual/residual.sum(1).sum(1)[:,None,None]     # note: square ROI assumed
        
        # joint probability of drawing new sampled x, y, z position and intensity
        q = np.zeros(n_eval)
        q[loc] = res_norm[np.arange(births),x_choice,y_choice] * u4


        # acceptance ratio = Posterior ratio * interpair selection probability ratio / P(u1, u2, u3, u4)
        A = np.zeros(n_eval)
        A[loc] = PR[loc] * self.p_death/self.p_birth / q[loc]

        # random number for accepting/rejecting the jumps
        u = np.random.uniform(0,1,n_eval)

        # Accept jumps where acceptance ratio > u with u ~ U(0,1)
        # update the estimates and model 
        failed_updates = (A < u) + np.isnan(A)        # where posterior ratio < random number AND where posterior ratio isNaN
        pos_update[failed_updates] = pos[failed_updates]
        I_update[failed_updates] = I[failed_updates]
        emitterState_update[failed_updates] = emitterState[failed_updates]
        ll_new[failed_updates] = ll_prev[failed_updates]
        prior_new[failed_updates] = prior_prev[failed_updates]
        
        return pos_update, I_update, backgroundParams, emitterState_update, ~failed_updates, ll_new, prior_new


class Death:
    """
    The death move randomly picks an emitter to remove from the estimate, 
    deactivating it in emitterState and setting its parameters to 0. Pairs with 
    birth.
    """
    def __init__(self, sigma_PSF, prior_func, LL_func, p_death, p_birth):
        self.sigma_PSF = sigma_PSF                  # PSF width assuming Gaussian or astigmatic PSFs. approximate/tune otherwise.
        self.prior_func = prior_func                # prior information
        self.LL_func = LL_func                      # Likelihood function
        self.p_birth = p_birth                      # probability of selecting birth from the birth/death pair
        self.p_death = p_death                      # probability of selecting death from the birth/death pair
        
    def calc(self, pos, I, backgroundParams, emitterState, k_max, smpIndices, ll_prev, prior_prev):
        """
        Executing this function generates a new model and parameters and 
        accepts or rejects them. 
        
        Parameters
        ----------
        pos :               array, [n_eval, k_max, dim]
                            Current position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                       
        I :                 array, [n_eval, k_max, numFrames]
                            Current emitter intensity for n_eval frames, up to k_max emitters, with numFrames shot in parallel.
                            
        backgroundParams :  array, [n_eval]
                            Current background photon count for n_eval frames. 
            
        emitterState :      array, [n_eval, k_max]
                            Currently active emitters for n_eval frames, up to k_max emitters.
                        
        k_max :             float
                            Maximum amount of active emitters.
                            
        smpIndices          array, [n_eval]
                            Informs Likelihood function which frames to use to calculate the Likelihood given the parameters.
                            
        ll_prev :           array, [n_eval]
                            Previous log Likelihood value, belonging to current parameters.
                            
        prior_prev :        array, [n_eval]
                            Previous prior probability value, belonging to current parameters.
        
        Returns
        -------
        pos_update :        array, [n_eval, k_max, dim]
                            Updated position estimates for n_eval frames, up to k_max emitters, in dim dimensions.
                        
        I_update :          array, [n_eval, k_max, numFrames]
                            Updated emitter intensity for n_eval frames, up to k_max emitters, with numFrames shot in parallel.
                            
        backgroundParams :  array, [n_eval]
                            Current background photon count for n_eval frames.
                            
        emitterState :      array, [n_eval, k_max]
                            Currently active emitters for n_eval frames, up to k_max emitters.
                            
        ~ failed_updates :  array, [n_eval]
                            Inverted failed updates returns an array of frames with a successful jump.
                            
        ll_new :            array, [n_eval]
                            New log Likelihood value, belonging to updated parameters.
                            
        prior_new :         array, [n_eval]
                            Previous prior probability value, belonging to current parameters.
        
        """
        
        modelParams = emitterState.sum(1)
        n_eval = len(modelParams)
        
        # return current model and parameters if no estimates are eligible for death
        deaths = len(modelParams[modelParams > 0])
        if deaths == 0:
            return pos, I, backgroundParams, emitterState, np.zeros(n_eval, dtype=bool), ll_prev, prior_prev 
        
        # frames eligible for death
        loc = np.where(modelParams > 0)[0]
        j1 = np.zeros(deaths, dtype = int)
        emitterState_update = np.copy(emitterState)
        
        # deactivate a random emitter
        for i in range(deaths):
            j1[i] = np.random.choice(np.arange(0, k_max)[emitterState[loc[i]]])     # pick elem for death, index j1 turns off
            emitterState_update[loc[i],j1[i]] = False
        
        # initialize position updates
        pos_update = np.copy(pos)
        I_update = np.copy(I)
        
        # set parameters of deactivated emitter to 0
        pos_update[loc,j1,0] = 0
        pos_update[loc,j1,1] = 0
        pos_update[loc,j1,2] = 0
        I_update[loc,j1,:] = 0
        
        # calculate the acceptance rate
        prior_new = self.prior_func.calc(pos_update, I_update, backgroundParams, emitterState_update, smpIndices)
        ll_new = self.LL_func.Likelihood(pos_update, I_update*emitterState_update[:,:,None], backgroundParams, smpIndices)

        # posterior ratio
        PR = np.exp(ll_new - ll_prev + prior_new - prior_prev)

        # acceptance ratio = Posterior ratio * interpair selection probability ratio
        A = np.zeros(n_eval)
        A[loc] = PR[loc] * self.p_birth/self.p_death
        
        # random number for accepting/rejecting the jumps
        u = np.random.uniform(0,1,n_eval)

        # Accept jumps where acceptance ratio > u with u ~ U(0,1)
        # update the estimates and model       
        failed_updates = (A < u) + np.isnan(A)        # where posterior ratio < random number AND where posterior ratio isNaN
        emitterState_update[failed_updates] = emitterState[failed_updates]
        ll_new[failed_updates] = ll_prev[failed_updates]
        prior_new[failed_updates] = prior_prev[failed_updates]
        
        return pos, I, backgroundParams, emitterState_update, ~failed_updates, ll_new, prior_new
       
        
def rjmcmc_3d_localization(move_list, move_prob, n_eval, numFrames,
                           pos_chain_0, I_chain_0, bg_chain_0, state_chain_0,
                           smpIndices, 
                           k_max = 10, chain_length=10000, ll_func=[], save_ev = False, roisize=20, print_preamble = ''):
    """
    This function executes a run of (RJ)MCMC, using the moves and move 
    selection probabilities provided in move_list and move_prob, respectively.
    It uses initial values pos_chain_0, I_chain_0, bg_chain_0, and 
    state_chain_0 to start the markov chains and run them up to chain_length. 
    The outputs are the chains of estimates as well as arrays containing 
    information for diagnosis and tuning.

    Parameters
    ----------
    move_list :         array of functions, [n_moves]
                        Array of moves to be used in this run.
                        
    move_prob :         array, [n_moves]
                        Array of move probabilities used to randomly sample moves in move_list.
                        
    n_eval :            float
                        Number of frames up for evaluation.
                        
    numFrames :         float
                        Number of frames shot in parallel. These frames share 1 estimate. Used in e.g. biplane imaging, multi-channel imaging.
                        
    pos_chain_0 :       array, [n_eval, k_max, dim]
                        Initial values for the Markov chain of position estimates.
                        
    I_chain_0 :         array, [n_eval, k_max, numFrames]
                        Initial values for the Markov chain of emitter intensity estimates.
    bg_chain_0 :        array, [n_eval]
                        Initial values for the Markov chain of background intensity estimates.
                        
    state_chain_0 :     array, [n_eval, k_max, dim]
                        Initial values for the Markov chain of active emitter estimates.
                        
    smpIndices :        array, [n_eval]
                        Informs Likelihood function which frames to use to calculate the Likelihood given the parameters.
                        
    k_max :             float
                        Maximum amount of active emitters.
                        The default is 10.
                        
    chain_length :      float
                        Number of iterations of the algorithm. 
                        The default is 10000.
                        
    ll_func :           function
                        Log Likelihood calculator function. used for additional diagnostic information, such as expected value per iteration.
                        The default is [].
                        
    save_ev :           bool
                        Flag for storing the expected value per iteration. Requires ll_func to be assigned.
                        The default is False.
    
    roisize :           int
                        Size of the region of interest, in pixels.
                        The default is 20.
                        
    print_preamble :    string
                        Additional print text during execution. For use in e.g. tracking loops of RJMCMC runs.
                        The default is ''.

    Returns
    -------
    pos_chain :         array, [n_eval, k_max, dim, chain_length]
                        Markov chain of position estimates.
        
    I_chain :           array, [n_eval, k_max, numFrames, chain_length] 
                        Markov chain of emitter intensity estimates.
     
    bg_chain :          array, [n_eval, chain_length] 
                        Markov chain of background intensity estimates.
                        
    state_chain :       array, [n_eval, k_max, chain_length] 
                        Markov chain of active emitters.
                        
    acceptance :        array, [n_eval, chain_length]
                        Array tracking the acceptance of proposed jumps over each iteration and on each frame.
                        
    move_tracker :      array, [chain_length]
                        Array tracking which move was proposed for each iteration.
                        
    ll_chain :          array, [n_eval, chain_length]
                        Log Likelihood of the estimate for each frame and each iteration, used to calculate the posterior ratio.
                        
    prior_chain :       array, [n_eval, chain_length]
                        Prior probability of the estimate for each frame and each iteration, used to calculate the posterior ratio.

    """
    
    # initialize position, intensity, background, and active emitter chains
    pos_chain   = np.zeros([n_eval, k_max, 3, chain_length])
    I_chain     = np.zeros([n_eval, k_max, numFrames, chain_length])
    bg_chain    = np.zeros([n_eval, chain_length])
    state_chain = np.zeros([n_eval, k_max, chain_length], dtype = bool)
    
    # initialize acceptance and move trackers
    acceptance      = np.zeros([n_eval, len(move_list), chain_length], dtype = bool)
    move_tracker    = np.zeros([chain_length], dtype=int)
    
    # initialize log likelihood and prior chain
    ll_chain = np.zeros([n_eval, chain_length])
    prior_chain = np.zeros([n_eval, chain_length])
    
    # load initial positions, intensities, backgrounds, and active emitters into the first iteration of the chains
    a1, a2                  = np.where(state_chain_0)
    pos_chain[a1,a2,:,0]    = pos_chain_0[a1, a2, :]
    I_chain[a1, a2, :, 0]   = I_chain_0[a1, a2, :]
    bg_chain[:,0]           = bg_chain_0
    state_chain[:,:,0]      = state_chain_0

    # calculate log likelihood and prior of the initial estimates and load them into the first iteration of their chains
    ll_current      = move_list[0].LL_func.Likelihood(pos_chain_0, I_chain_0 * state_chain_0[:,:,None], bg_chain_0, smpIndices)
    prior_current   = move_list[0].prior_func.calc(pos_chain_0, I_chain_0, bg_chain_0, state_chain_0, smpIndices)
    ll_chain[:,0] = ll_current
    prior_chain[:,0] = prior_current
    
    # if additional diagnostics are required, make an expected value chain and calculate and store the expected value of the initial estimate
    if save_ev:
        ev_chain = np.zeros([n_eval, roisize, roisize, chain_length])
        ev_chain[:,:,:,0] = (ll_func.ExpectedValue(pos_chain[:,:,:,0], I_chain[:,:,:,0]*state_chain[:,:,None,0]) + bg_chain[:,0,None,None,None]).transpose([0,2,3,1]).squeeze()
    
    # for the required number of iterations, pick a random move to execute and update the Markov chains
    for i in range(chain_length-1):
        
        # move selection
        a = np.random.choice(np.arange(len(move_list)), p = move_prob)      # for logging the selected jumps and acceptance rates
        move = move_list[a]
        move_tracker[i] = a
        
        # execute the jump
        pos_chain[:,:,:,i+1], I_chain[:,:,:,i+1], bg_chain[:,i+1], state_chain[:,:,i+1], acceptance[:,a,i], ll_chain[:,i+1], prior_chain[:,i+1] = move.calc(pos_chain[:,:,:,i], I_chain[:,:,:,i], bg_chain[:,i], state_chain[:,:,i], k_max, smpIndices, ll_chain[:,i], prior_chain[:,i])

        if save_ev:
            ev_chain[:,:,:,i+1] = (ll_func.ExpectedValue(pos_chain[:,:,:,i+1], I_chain[:,:,:,i+1]*state_chain[:,:,None,i+1]) + bg_chain[:,i+1,None,None,None]).transpose([0,2,3,1]).squeeze()

        # print an update every x iterations
        if np.mod(i,200) == 0:           
            print(print_preamble + f'iteration: {i}')# | moves: {moves_accepted}/{moves} | splits: {splits_accepted}/{splits} | merges: {merges_accepted}/{merges} | births: {births_accepted}/{births} | deaths: {deaths_accepted}/{deaths} ')

    # return additional expected value chain
    if save_ev:
        return pos_chain, I_chain, bg_chain, state_chain, acceptance, move_tracker, ll_chain, prior_chain, ev_chain
    
    # return chains and trackers
    else:
        return pos_chain, I_chain, bg_chain, state_chain, acceptance, move_tracker, ll_chain, prior_chain
    

class MCMC:
    def __init__(self):
        self.name = 'mcmc_parameter/result_object'
        
        
class RJMCMC:
    def __init__(self):
        self.name = 'rjmcmc_parameter/result_object'       


class RJMCMC_Object:
    """
    Main class for running the RJMCMC 3D localization algorithm. The class is
    initalized with the PSF model and image data. Localization is executed as
    follows:
        - run the RJMCMC algorithm with (random) initial estimates
        - discard the burn-in portion of the RJMCMC run and determine
            the maximum a posteriori estimate of the model
        - run MCMC, now using the MAP model and initializing the parameters
            from the last RJMCMC estimate with the MAP number of emitters
        - store the chains for further processing
    The images and estimates may then be reconstructed using histograms and 
    determining the means of the chains.
    
    Function set_priors is used to build the prior calculator, assigning
    all PDF arrays. It also initializes the MCLocalizer, assigning the samples
    and limits.
    
    Function set_rjmcmc_moves assigns the rjmcmc moves and hyperparameters
    as well as an object for storing the chains. It also forms the move_list
    and move_prob used when calling the rjmcmc_3d_localization function.
    
    Function set_mcmc_moves works similarly to set_rjmcmc_moves, except for
    exclusively assigning parameter space moves. This also allows the parameter
    space moves to change hyperparameters between RJMCMC and MCMC runs.
    
    Function run_rjmcmc executes the RJMCMC portion of the localization 
    algorithm using the previously set priors and rjmcmc moves.
    
    Function select_MAPN determines the model by selecting the maximum a 
    posteriori estimate of number of emitters after discarding the burn-in 
    portion of the RJMCMC chain.
    
    Function run_mcmc executes the MCMC portion of the localization algorithm
    using previously set priors and mcmc moves. The MAP number of emitters  
    determines the model and the last valid RJMCMC estimate of that model
    determines the initial estimate.
    
    """
    def __init__(self, ctx, psf, smp, z_range, sigma_PSF, z_rescale = 1):
        self.ctx = ctx                                          # photonpy library context
        self.psf = psf                                          # photonpy point spread function object
        self.smp = smp                                          # image data/samples
        self.n_eval = smp.shape[0]                              # number of frames
        self.numFrames = smp.shape[1]                           # number of frames shot in 'parallel' (biplane, multichannel imaging)
        self.roisize = smp.shape[2]                             # size of the region of interest
        self.z_range = z_range                                  # array containing the minimum and maximum emitter axial distance from focal plane
        self.sigma_PSF = sigma_PSF                              # point spread function width (Gaussian, astigmatic), or approximate width (tetrapod, other imaging modalities)
        self.smpIndices = np.arange(self.n_eval, dtype = int)   # array informing likelihood funciton which frames to use for calculations
        self.z_rescale = z_rescale                              # float to flatten/stretch clustering sphere in z direction
        
    def set_priors(self, border = 4, k_max = 10, mu_k = 5, bg_offset_range = [3, 30], 
                   mu_I = 2000, sigma_I = 150, I_taper = 500, frac = 40, 
                   I_PDF_from_SMLM = False):
        """
        Sets all the priors: 
            - emitter count from a Poisson distribution
            - background offset from uniform distribution
            - emitter intensity either from SMLM kernel density
                fit or from user defined gaussian
            - emitter position from uniform distribution
        and makes the prior + Likelihood calculators.

        PDFs can be overwritten after calling the function, as long as the 
        prior calculator is also remade.
        
        Parameters
        ----------
        border :            float
                            Additional pixels outside the region of interest to consider for position estimates.
                            The default is 4.
                        
        k_max :             float
                            Maximum amount of active emitters.
                            The default is 10.
                        
        mu_k :              float
                            Input for the Poisson distribution of the number of emitters. 
                            The default is 5.
        
        bg_offset_range :   array, [2]
                            Borders for the uniform prior for background intensity.
            
        mu_I :              float
                            Mean of Gaussian distribution used in forming the intensity prior.
                            The default is 2000.
                            
        sigma_I :           float
                            Standard deviation of Gaussian distribution used in forming the intensity prior.
                            The default is 150.
                            
        I_taper :           float
                            Additional parameter for forming the intensity prior.
                            The default is 500.
                            
        frac :              float
                            Additional parameter for forming the intensity prior.
                            The default is 40.
                            
        I_PDF_from_SMLM :   OBSOLETE

        Returns
        -------
        None.

        """
        
        # assign parameters
        self.k_max = k_max
        self.mu_k = mu_k
        self.border = border
        self.k_PDF = poisson._pmf(np.arange(0, k_max + 1,), mu_k)
        self.bg_PDF = np.zeros(10000)
        self.bg_PDF[bg_offset_range[0]: bg_offset_range[1]] = 1
        self.bg_PDF /= self.bg_PDF.sum()
        
        #if I_PDF_from_SMLM:
        #    #KernelDensity( bandwidth = 75, kernel = 'gaussian')
        #    return 0
        #else:
        #sI = 500
        #self.I_PDF = gauss1d(np.arange(30000), mu_I, sI)
        #self.I_PDF[int(mu_I + 3*sI)::] = 0
        
        # make intensity PDF, Gaussian with 'shoulder' to the left
        self.I_PDF = gauss1d(np.arange(30000), mu_I, sigma_I)
        self.I_PDF[int(mu_I + 3*sigma_I)::] = 0
        self.I_PDF[I_taper:np.where(self.I_PDF > max(self.I_PDF)/frac)[0][0]] = max(self.I_PDF)/frac
        self.I_PDF[0:I_taper] = np.linspace(0,max(self.I_PDF)/frac, I_taper)
        self.I_PDF /= self.I_PDF.sum()
            
        # make the prior calculator using the available parameters and PDFs
        self.p_calc = Prior_calculator(self.n_eval, prior_I, self.I_PDF, prior_bg, self.bg_PDF,
                                       prior_xyz, self.roisize, self.border, self.z_range[0], 
                                       self.z_range[1], prior_k, self.k_PDF, self.mu_k, 
                                       self.k_max)
        
        # make the MCLocalizer object and assign samples 
        self.intensityPDF = PDFInfo(self.I_PDF[100:3000],100, 3000)    
        self.mcl = MCLocalizer(self.k_max, self.numFrames, self.psf, self.intensityPDF, self.ctx)
        self.mcl.SetSamples(self.smp, self.smp*0)      

    def set_rjmcmc_moves(self, move_prob = np.array((3,3,3,1,1,1,1,1,1))/15, 
                         move_prob_2 = np.array((4,4,4,0,0,1.5,1.5,.5,.5))/16, 
                         p_split = .5, p_gsplit = .5, p_birth = .5,
                         sigma_x = 0.05, sigma_y = 0.05, sigma_z = 0.01,
                         sigma_I = 5, sigma_bg = 1,
                         rjmcmc_len = 5000, rjmcmc_burn = 3000):
        """
        Generates all the moves and required parameters using BAMF's 
        probabilities for move selection as default in burn-in and post burn-in
        phase.

        Parameters
        ----------
        move_prob :     array, [n_moves]
                        Array of move probabilities used to randomly sample moves in move_list. Used in burn-in portion.
                        The default is np.array((3,3,3,1,1,1,1,1,1))/15.
                        
        move_prob_2 :   array, [n_moves]
                        Array of move probabilities used to randomly sample moves in move_list. Used after burn-in is finished.
                        The default is np.array((4,4,4,0,0,1.5,1.5,.5,.5))/16.
                        
        p_split :       float
                        Probability of selecting split over merge.
                        The default is .5.
                        
        p_gsplit :      float
                        Probability of selecting generalized split over generalized merge.
                        The default is .5.
                        
        p_birth :       float
                        Probability of selecting birth over death.
                        The default is .5.
                        
        sigma_x :       float
                        Random walk standard deviation for jumps in x position.
                        The default is 0.05.
                        
        sigma_y :       float
                        Random walk standard deviation for jumps in y position.
                        The default is 0.05.
                        
        sigma_z :       float
                        Random walk standard deviation for jumps in z position.
                        The default is 0.01.
                        
        sigma_I :       float
                        Random walk standard deviation for jumps in emitter intensity.
                        The default is 5.
                        
        sigma_bg :      float
                        Random walk standard deviation for jumps in background intensity.
                        The default is 1.
                        
        rjmcmc_len :    int
                        Total number of iterations for the RJMCMC run, including burn-in.
                        The default is 5000.
                        
        rjmcmc_burn :   int
                        Number of burn-in iterations < rjmcmc_len.
                        The default is 3000.

        Returns
        -------
        None.

        """

        # assign result object, move probabilities, chain lengths, hyperparameters for jumps
        self.rjmcmc = RJMCMC()
        
        self.move_prob      = move_prob
        self.move_prob_2    = move_prob_2
        
        self.rjmcmc_len     = rjmcmc_len
        self.rjmcmc_burn    = rjmcmc_burn
        
        self.sigma_x    = sigma_x
        self.sigma_y    = sigma_y
        self.sigma_z    = sigma_z
        self.sigma_I    = sigma_I
        self.sigma_z    = sigma_z
        self.sigma_bg   = sigma_bg
        
        # make the model space move pairs
        self.split_move     = Split(self.sigma_PSF, self.p_calc, self.mcl, p_split, 1-p_split, z_rescale = self.z_rescale)
        self.merge_move     = Merge(self.sigma_PSF, self.p_calc, self.mcl, 1-p_split, p_split, z_rescale = self.z_rescale)
        self.gsplit_move    = Generalized_Split(self.sigma_PSF, self.p_calc, self.mcl, p_gsplit, 1-p_gsplit, z_rescale = self.z_rescale)
        self.gmerge_move    = Generalized_Merge(self.sigma_PSF, self.p_calc, self.mcl, 1-p_gsplit, p_gsplit, z_rescale = self.z_rescale)
        self.birth_move     = Birth(self.sigma_PSF, self.p_calc, self.mcl, p_birth, 1-p_birth, z_range = self.z_range)
        self.death_move     = Death(self.sigma_PSF, self.p_calc, self.mcl, 1-p_birth, p_birth)
        
        # make the parameter space moves
        self.single_move = Single_Emitter_Move(sigma_x, sigma_y, sigma_z, sigma_I, self.p_calc, self.mcl)
        self.bg_move = Background_Move(sigma_bg, 1e-9, 1e-9, self.p_calc, self.mcl)
        self.group_move_uncon = Group_Move_Unconserved(self.sigma_PSF, self.sigma_x, self.sigma_y, 
                                                       self.sigma_z, self.sigma_I, self.p_calc, self.mcl)
        
        # name the moves
        self.split_move.name = 'split'
        self.merge_move.name = 'merge'
        self.gsplit_move.name = 'gsplit'
        self.gmerge_move.name = 'gmerge'
        self.birth_move.name = 'birth'
        self.death_move.name = 'death'
        self.single_move.name = 'single'
        self.bg_move.name = 'bg'
        self.group_move_uncon.name = 'group_uncon'
    
        # enter the moves into a list
        self.move_list = [self.group_move_uncon, self.single_move, self.bg_move, self.split_move, 
                          self.merge_move, self.gsplit_move, self.gmerge_move, self.birth_move, 
                          self.death_move]

        
    def set_mcmc_moves(self, move_prob_mcmc = [.4, .4, .2], mcmc_len = 3000, mcmc_burn = 0,
                       sigma_x = 0.05, sigma_y = 0.05, sigma_z = 0.01,
                       sigma_I = 5, sigma_bg = 1):
        """
        Generates separate moves for the MCMC portion of
        the algorithm.

        Parameters
        ----------
        move_prob_mcmc :    array, [n_moves]
                            Array of move probabilities used to randomly sample moves in move_list.
                            The default is [.4, .4, .2].
                            
        mcmc_len :          int
                            Total number of iterations for the MCMC run, including burn-in. 
                            The default is 3000.
                            
        mcmc_burn :         int
                            Number of burn-in iterations < mcmc_len. 
                            The default is 0.
                                
        sigma_x :           float
                            Random walk standard deviation for jumps in x position.
                            The default is 0.05.
                        
        sigma_y :           float
                            Random walk standard deviation for jumps in y position.
                            The default is 0.05.
                        
        sigma_x :           float
                            Random walk standard deviation for jumps in z position.
                            The default is 0.01.
                        
        sigma_I :           float
                            Random walk standard deviation for jumps in emitter intensity.
                            The default is 5.
                        
        sigma_bg :          float
                            Random walk standard deviation for jumps in background intensity.
                            The default is 1.

        Returns
        -------
        None.

        """
        
        # assign result object, move probabilities, chain lengths, hyperparameters for jumps
        self.mcmc = MCMC()
        
        self.mcmc.move_prob = move_prob_mcmc
        
        self.mcmc_len   = mcmc_len
        self.mcmc_burn  = mcmc_burn
        
        self.mcmc.sigma_x   = sigma_x
        self.mcmc.sigma_y   = sigma_y
        self.mcmc.sigma_z   = sigma_z
        self.mcmc.sigma_I   = sigma_I
        self.mcmc.sigma_z   = sigma_z
        self.mcmc.sigma_bg  = sigma_bg
        
        # make the parameter space moves
        self.mcmc.single_move = Single_Emitter_Move(sigma_x, sigma_y, sigma_z, sigma_I, self.p_calc, self.mcl)
        self.mcmc.bg_move = Background_Move(sigma_bg, 1e-9, 1e-9, self.p_calc, self.mcl)
        self.mcmc.group_move_uncon = Group_Move_Unconserved(self.sigma_PSF, self.sigma_x, self.sigma_y, 
                                                       self.sigma_z, self.sigma_I, self.p_calc, self.mcl)
        
        # name the moves
        self.mcmc.single_move.name      = 'single_mcmc'
        self.mcmc.bg_move.name          = 'bg_mcmc'
        self.mcmc.group_move_uncon.name = 'group_uncon_mcmc'
        
        # enter the moves into a list
        self.mcmc.move_list = [self.mcmc.group_move_uncon, self.mcmc.single_move, self.mcmc.bg_move]


    def run_rjmcmc(self, save_ev = False, print_preamble = '', **kwargs):
        """
        runs RJMCMC on the data using a burn-in portion with high model 
        move selection probability and a post burn-in portion which
        uses a different move selection probability

        Parameters
        ----------
        save_ev :           bool
                            Flag for storing the expected value per iteration. Requires ll_func to be assigned.
                            The default is False.
                    
        print_preamble :    string
                            Additional print text during execution. For use in e.g. tracking loops of RJMCMC runs.
                            The default is ''.
                            
        **kwargs :          keyword-value pair
                            Used to load in non-random initial estimates.

        Returns
        -------
        None.

        """

        # randomly draw an initial localization from the priors, turning only 1 emitter on in each frame
        pos_chain_0 = np.random.uniform(0, self.roisize, size = [self.n_eval, self.k_max, 3])
        pos_chain_0[:,:,-1] = np.random.uniform(self.z_range[0], self.z_range[1], [self.n_eval, self.k_max])
        
        # initialize emitter intensity from I_PDF
        I_chain_0 = np.random.choice(np.arange(len(self.I_PDF)), p = self.I_PDF, size = [self.n_eval, self.k_max, self.numFrames])

        # initialize background intensity from bg_PDF
        bg_chain_0 = np.random.choice(np.arange(len(self.bg_PDF)), p = self.bg_PDF, size = [self.n_eval])
        
        # initialize emitter state chain and activate the first emitter
        state_chain_0 = np.zeros([self.n_eval, self.k_max], dtype=bool)
        state_chain_0[:,0] = True
        pos_chain_0 *= state_chain_0[:,:,None]
        I_chain_0 *= state_chain_0[:,:,None]

        # overwrite random initial estimates
        if len(kwargs) > 0:
            try:
                pos_chain_0 = kwargs['pos_0']
            except:
                pass
            try:
                state_chain_0 = kwargs['state_0']
            except:
                pass  
            try:
                I_chain_0 = kwargs['I_0']
            except:
                pass
            try:
                bg_chain_0 = kwargs['bg_0']
            except:
                pass
        
        # run RJMCMC burn-in portion by calling the rjmcmc_3d_localization function
        # store the chains in self.rjmcmc.***_chain_burn
        print('running burn-in portion of RJMCMC')
        if save_ev:
                self.rjmcmc.pos_chain_burn, self.rjmcmc.I_chain_burn, self.rjmcmc.bg_chain_burn, self.rjmcmc.state_chain_burn, self.rjmcmc.acceptance_burn, self.rjmcmc.move_tracker_burn, self.rjmcmc.ll_chain_burn, self.rjmcmc.prior_chain_burn, self.rjmcmc.ev_chain_burn = rjmcmc_3d_localization(self.move_list, self.move_prob, self.n_eval, self.numFrames,
                                                               pos_chain_0, I_chain_0, bg_chain_0, state_chain_0,
                                                               self.smpIndices, 
                                                               k_max = self.k_max, chain_length=self.rjmcmc_burn, ll_func = self.mcl, save_ev = True, roisize = self.roisize, print_preamble = print_preamble)
        else:
            self.rjmcmc.pos_chain_burn, self.rjmcmc.I_chain_burn, self.rjmcmc.bg_chain_burn, self.rjmcmc.state_chain_burn, self.rjmcmc.acceptance_burn, self.rjmcmc.move_tracker_burn, self.rjmcmc.ll_chain_burn, self.rjmcmc.prior_chain_burn = rjmcmc_3d_localization(self.move_list, self.move_prob, self.n_eval, self.numFrames,
                                                               pos_chain_0, I_chain_0, bg_chain_0, state_chain_0,
                                                               self.smpIndices, 
                                                               k_max = self.k_max, chain_length=self.rjmcmc_burn, ll_func = self.mcl, roisize=self.roisize, print_preamble = print_preamble)

        # run RJMCMC post burn-in, changing the hyperparameters
        # store the chains in self.rjmcmc.***_chain
        print('running post burn-in RJMCMC')
        if save_ev:
            self.rjmcmc.pos_chain, self.rjmcmc.I_chain, self.rjmcmc.bg_chain, self.rjmcmc.state_chain, self.rjmcmc.acceptance, self.rjmcmc.move_tracker, self.rjmcmc.ll_chain, self.rjmcmc.prior_chain, self.rjmcmc.ev_chain =      rjmcmc_3d_localization(self.move_list, self.move_prob_2, self.n_eval, self.numFrames,
                                                               self.rjmcmc.pos_chain_burn[:,:,:,-1], self.rjmcmc.I_chain_burn[:,:,:,-1],
                                                               self.rjmcmc.bg_chain_burn[:,-1], self.rjmcmc.state_chain_burn[:,:,-1],
                                                               self.smpIndices, 
                                                               k_max = self.k_max, chain_length=self.rjmcmc_len - self.rjmcmc_burn, ll_func = self.mcl, save_ev = True, roisize = self.roisize, print_preamble = print_preamble)
        else:
            self.rjmcmc.pos_chain, self.rjmcmc.I_chain, self.rjmcmc.bg_chain, self.rjmcmc.state_chain, self.rjmcmc.acceptance, self.rjmcmc.move_tracker, self.rjmcmc.ll_chain, self.rjmcmc.prior_chain =      rjmcmc_3d_localization(self.move_list, self.move_prob_2, self.n_eval, self.numFrames,
                                                               self.rjmcmc.pos_chain_burn[:,:,:,-1], self.rjmcmc.I_chain_burn[:,:,:,-1],
                                                               self.rjmcmc.bg_chain_burn[:,-1], self.rjmcmc.state_chain_burn[:,:,-1],
                                                               self.smpIndices, 
                                                               k_max = self.k_max, chain_length=self.rjmcmc_len - self.rjmcmc_burn, ll_func = self.mcl, roisize=self.roisize, print_preamble = print_preamble)           

    def select_MAPN(self):
        """
        Select the optimal model: number of emitters with the highest maximum a 
        posteriori probability.
        """
        
        print('finding MAP number of emitters')   
        
        # initialize emitter model and MCMC initial estimates
        self.found_emitters = np.zeros(self.n_eval, dtype = int)
        self.mcmc.pos_0     = np.zeros([self.n_eval, self.k_max, 3])
        self.mcmc.I_0       = np.zeros([self.n_eval, self.k_max, self.numFrames])
        self.mcmc.bg_0      = np.zeros(self.n_eval)
        self.mcmc.state_0   = np.zeros([self.n_eval, self.k_max], dtype = bool)

        # for each frame, find the MAP number of emitters and find the last corresponding estimate
        for i in range(self.n_eval):
            unique, counts = np.unique(self.rjmcmc.state_chain.sum(1)[i], return_counts = True)
            
            # MAP number of emitters
            self.found_emitters[i]  = unique[counts == max(counts)]
            
            # last RJMCMC estimate from the MAP model
            self.mcmc.pos_0[i,:,:]  = self.rjmcmc.pos_chain[i,:,:,self.rjmcmc.state_chain.sum(1)[i] == self.found_emitters[i]].transpose((1,2,0))[:,:,-1]
            self.mcmc.I_0[i,:,:]    = self.rjmcmc.I_chain[i,:,:,self.rjmcmc.state_chain.sum(1)[i] == self.found_emitters[i]].transpose((1,2,0))[:,:,-1]
            self.mcmc.bg_0[i]       = self.rjmcmc.bg_chain[i, self.rjmcmc.state_chain.sum(1)[i] == self.found_emitters[i]][-1]
            self.mcmc.state_0[i,:]  = self.rjmcmc.state_chain[i, :, self.rjmcmc.state_chain.sum(1)[i] == self.found_emitters[i]].transpose()[:,-1]
           
         
    def run_mcmc(self, save_ev = False, print_preamble = ''):
        """
        runs MCMC on the most likely model of number of emitters
        """
        
        # run MCMC
        # store the chains in self.mcmc.***_chain
        print('running MCMC')
        if save_ev:
            self.mcmc.pos_chain, self.mcmc.I_chain, self.mcmc.bg_chain, self.mcmc.state_chain, self.mcmc.acceptance, self.mcmc.move_tracker, self.mcmc.ll_chain, self.mcmc.prior_chain, self.mcmc.ev_chain =      rjmcmc_3d_localization(self.mcmc.move_list, self.mcmc.move_prob, self.n_eval, self.numFrames,
                                                               self.mcmc.pos_0, self.mcmc.I_0,
                                                               self.mcmc.bg_0, self.mcmc.state_0,
                                                               self.smpIndices, 
                                                               k_max = self.k_max, chain_length=self.mcmc_len, ll_func = self.mcl, save_ev = True, roisize=self.roisize, print_preamble = print_preamble)
        else:
            self.mcmc.pos_chain, self.mcmc.I_chain, self.mcmc.bg_chain, self.mcmc.state_chain, self.mcmc.acceptance, self.mcmc.move_tracker, self.mcmc.ll_chain, self.mcmc.prior_chain =      rjmcmc_3d_localization(self.mcmc.move_list, self.mcmc.move_prob, self.n_eval, self.numFrames,
                                                               self.mcmc.pos_0, self.mcmc.I_0,
                                                               self.mcmc.bg_0, self.mcmc.state_0,
                                                               self.smpIndices, 
                                                               k_max = self.k_max, chain_length=self.mcmc_len, ll_func = self.mcl, roisize=self.roisize, print_preamble = print_preamble)
        

    
